//
//  CriteriaViewController.swift
//  Choyces
//
//  Created by Amanda Cosel on 5/16/17.
//  Copyright © 2017 DCApps. All rights reserved.
//

import UIKit
import Firebase
import DraggableTableView

//var criteria : [Criterion] = []




class CriteriaViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIPopoverPresentationControllerDelegate, DragableTableDelegate  {
    
   
    
    //OUTLETS
    @IBOutlet weak var prioritizeInfoView: UIView!
    //@IBOutlet weak var tooTipButton: UIButton!
    
    @IBOutlet weak var factorNameLabel: UILabel!
    @IBOutlet weak var addCriteriaView: UIView!
    @IBOutlet weak var noCriteriaView: UIView!
   
    @IBOutlet weak var noCriteriaImage: UIImageView!
    
    @IBOutlet weak var criteriaTable: UITableView!
    
    @IBOutlet weak var addCriteriaTextField: UITextField!
    
    @IBOutlet weak var addCriteriaButton: UIButton!
    
    @IBOutlet weak var grayLine: UIView!
    
    //LOCAL VARIABLES
    
     var selectedIndexPath: IndexPath? = nil
    let ref = FIRDatabase.database().reference().child("Users")
    
  
   //var criteria : [Criterion] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        criteriaTable.delegate = self
        criteriaTable.dataSource = self
        if progress == 3{
        
        self.criteriaTable.dragable = true
            
        } else {
            self.criteriaTable.dragable = false
        }
        criteriaTable.dragableDelegate = self
      
        
        /*
        let origToolTipImage = UIImage(named: "Info-50.png")
        let tintedToolTipImage = origToolTipImage?.withRenderingMode(.alwaysTemplate)
        tooTipButton?.setImage(tintedToolTipImage, for: .normal)
        tooTipButton?.tintColor = UIColor.lightGray
        addCriteriaButton.isEnabled = false
        
 */
 
         self.criteriaTable.allowsMultipleSelectionDuringEditing = false
        
        
        
        if usePreLoadedFactors == nil && (((currentDecision?.choices.contains(where: {$0.name == "American Food"}))! || (currentDecision?.choices.contains(where: {$0.name == "Chineese Food"}))! || (currentDecision?.choices.contains(where: {$0.name == "Mexican Food"}))! || (currentDecision?.choices.contains(where: {$0.name == "Italian Food"}))!) || (currentDecision?.name.contains("eat"))! || (currentDecision?.name.contains("food"))!) {
            
            usePreLoadedFactors = true
            currentDecision?.criteria = []
            let criteriaOne = Criterion(name: "Price")
            let criteriaTwo = Criterion(name: "Taste")
            currentDecision?.criteria.append(criteriaOne)
            currentDecision?.criteria.append(criteriaTwo)
            factorsProgress = 0.1
        
        } else {}
        
        noCriteria()
        
        //decisionNameLabels.text = "\(currentDecision?.name)"
        
        if (currentDecision?.criteria.count)! > 0 {
        let firstCriterion = IndexPath(row: 0, section: 0)
        let lastCriterion = IndexPath(row:(currentDecision?.criteria.count)!-1, section: 0)
        prioritizeCriteria(firstCriterion: firstCriterion, lastCriterion: lastCriterion)
        criteriaTable.reloadData()
          
            
           
        
        } else {
            
         
            
            
            
            
        }
        
       
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if progress == 2 {
        criteriaTable.isEditing = false
            addCriteriaTextField.isHidden = false
            addCriteriaButton.isHidden = false
            grayLine.isHidden = false
            prioritizeInfoView.isHidden = true
       
            
        } else {
            criteriaTable.isEditing = true
            addCriteriaTextField.isHidden = true
            addCriteriaButton.isHidden = true
            grayLine.isHidden = true
            factorNameLabel.text = currentDecision?.criteria[0].name
            prioritizeInfoView.isHidden = false
            
        }
        
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    func noCriteria() {
        if (currentDecision?.criteria.count)! > 0 {
            noCriteriaView.isHidden = true
            
        } else {
            noCriteriaImage?.image = noCriteriaImage?.image!.withRenderingMode(.alwaysTemplate)
            noCriteriaImage?.tintColor = UIColor.lightGray
            noCriteriaView.isHidden = false
            
        }
        
        
        
        
    }
    
    /*
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToTip" {
            
            let popoverViewController = segue.destination
            popoverViewController.popoverPresentationController?.delegate = self
            let vc = segue.destination as? ToolTIpViewController
            let pop = vc?.popoverPresentationController
            pop?.delegate = vc as UIPopoverPresentationControllerDelegate?
            
        }
        
    }

    
*/
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentDecision!.criteria.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "criteriaCell") as! CriteriaTableViewCell
        let criterion = currentDecision?.criteria[indexPath.row]
        
        cell.criteriaNameLabel.text = (criterion?.name)!
        cell.criteriaPriorityLabel.text = "\(((currentDecision?.criteria.count)! - (criterion?.priority)!) + 1)"
        
       // cell.deleteCriteriaButton.addTarget(self,action:#selector(deleteCriteria(sender:)), for: .touchUpInside)
        
        
        
        if indexPath.row == ((currentDecision?.criteria.count)! - 1) {
            
            
            
            cell.grayLine.isHidden = true
           // cell.whiteLine.isHidden = true
        } else {
            cell.grayLine.isHidden = false
           // cell.whiteLine.isHidden = false
            
        }
        
        if progress == 2 {
          
            cell.priorityView.isHidden = true
            
        } else {
         
            cell.priorityView.isHidden = false
        }

        
        
        return cell
    }
    
    
    func deleteCriteria(sender:UIButton) {
        
        let buttonRow = sender.tag
        
        currentDecision?.criteria.remove(at: buttonRow)
        
        self.viewDidLoad()
        
        criteriaTable.reloadData()
        
        noCriteria()
    
        
        
    }

    
    
    @IBAction func criteriaButtonTapped(_ sender: Any) {
        
        var newCriteria = Criterion(name: "\(addCriteriaTextField.text!)")
        
        newCriteria.isSelected = true
        
        let newCriteriaRef = self.ref.child((activeUser?.uid)!).child("factors").child((addCriteriaTextField.text?.lowercased())!)
        
        newCriteriaRef.setValue(newCriteria.toAnyObject())
        
        currentDecision?.criteria.append(newCriteria)
        
        self.viewDidLoad()
        
        criteriaTable.reloadData()
        
        
        factorsProgress = factorsProgress + 0.05
        
        addCriteriaTextField.text = ""
        
        addCriteriaButton.isEnabled = false
        NotificationCenter.default.post(name: .reload, object: nil)
        noCriteria()
        
    }
    
    
    func tableView(_ tableView: UITableView, dragCellFrom fromIndexPath: IndexPath, toIndexPath: IndexPath) {
        
        
        
        var itemToMove = currentDecision?.criteria[fromIndexPath.row]
                currentDecision?.criteria.remove(at: fromIndexPath.row)
                currentDecision?.criteria.insert(itemToMove!, at: toIndexPath.row)
                let firstCriterion = IndexPath(row: 0, section: 0)
                let lastCriterion = IndexPath(row:(currentDecision?.criteria.count)!-1, section: 0)
                prioritizeCriteria(firstCriterion: firstCriterion, lastCriterion: lastCriterion)
                criteriaTable.reloadData()
                factorNameLabel.text = currentDecision?.criteria[0].name
        
        
    }
    

    
//    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
//        
//        
//        return true
//    }
//    
//    
//    
//    
//    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
//        var itemToMove = currentDecision?.criteria[sourceIndexPath.row]
//        currentDecision?.criteria.remove(at: sourceIndexPath.row)
//        currentDecision?.criteria.insert(itemToMove!, at: destinationIndexPath.row)
//        let firstCriterion = IndexPath(row: 0, section: 0)
//        let lastCriterion = IndexPath(row:(currentDecision?.criteria.count)!-1, section: 0)
//        prioritizeCriteria(firstCriterion: firstCriterion, lastCriterion: lastCriterion)
//        criteriaTable.reloadData()
//        factorNameLabel.text = currentDecision?.criteria[0].name
//        
//    }
    
    func prioritizeCriteria ( firstCriterion: IndexPath, lastCriterion: IndexPath) {
        
        for index in firstCriterion.row...lastCriterion.row {
            currentDecision?.criteria[index].priority =
                (currentDecision?.criteria.count)! - index
            
            print(currentDecision?.criteria[index].priority)
            
            
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        
        if progress == 2 {
            
            return .delete
        } else {
        
            return .none
    
        }
       
    }
    
 
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        var newCriteria = Criterion(name: "\(addCriteriaTextField.text)")
        
        currentDecision?.criteria.append(newCriteria)
        
        self.viewDidLoad()
        
        criteriaTable.reloadData()
        
        addCriteriaTextField.resignFirstResponder()
        return (true)
        
    }
    
    
    
    @IBAction func criteriaTextChanged(_ sender: Any) {
        
        if addCriteriaTextField.text == "" {
            
            addCriteriaButton.isEnabled = false
            
        } else {
            
            addCriteriaButton.isEnabled = true
            
        }

        
        
    }
    
    /*

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
        switch(selectedIndexPath)
        {
            
        case nil:
            
            let cell = criteriaTable.cellForRow(at: indexPath) as! CriteriaTableViewCell
            // cell.arrowImagem.image = UIImage(named:"content_arrow2")
            selectedIndexPath = indexPath
            cell.backgroundColor = UIColor.white
            
            
            
            
        default:
            if selectedIndexPath == indexPath
            {
                
                let cell = criteriaTable.cellForRow(at: indexPath) as! CriteriaTableViewCell
                //cell.arrowImagem.image = UIImage(named:"content_arrow")
                //cell.reloadInputViews()
                selectedIndexPath = nil
                cell.backgroundColor = UIColor.white
               
                
            }
            
            
        }
        
        //self.myTableView.beginUpdates()
        
        self.criteriaTable.reloadData()
        
        
        //myTableView.reloadRows(at: [indexPath], with: .automatic)
        
        self.criteriaTable.endUpdates()
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        
        
        let index = indexPath
        
        if selectedIndexPath != nil{
            if(index == selectedIndexPath)
            {
                
                return 160
            }
            else{
                return 70
            }
        }
        else
        {
            return 70
        }
        
        
        
    }
    */
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        
        
        
        var deleteItRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.default , title: "Delete",  handler:{action, indexpath in
            print("DELETE•ACTION");
            
            
            
            let index = indexPath.row
            currentDecision?.criteria.remove(at: index)
            self.viewDidLoad()
            
            self.criteriaTable.reloadData()
            
            factorsProgress = factorsProgress - 0.05
            NotificationCenter.default.post(name: .reload, object: nil)
            self.noCriteria()
            
            
            
            
            
            
        });
        
        
        
        
        
        
        
        
        
        deleteItRowAction.backgroundColor = UIColor(colorLiteralRed: 227/255, green: 227/255, blue: 227/255, alpha: 1)
        
        
        return [deleteItRowAction];
        
    }



    
    
    /*
    
    @IBAction func prioritizeButtonTapped(_ sender: Any) {
        
        editMode()
        
    }
    
    func editMode() {
        if criteriaTable.isEditing == true {
            
            criteriaTable.isEditing = false
            
            
          
        } else {
           
           criteriaTable.isEditing = true
    
          
        }
    }

*/

    
    
  

}
