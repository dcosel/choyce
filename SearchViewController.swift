//
//  SearchViewController.swift
//  Choyces
//
//  Created by Amanda Cosel on 6/4/17.
//  Copyright © 2017 DCApps. All rights reserved.
//

import UIKit
import Firebase


var factorsToAdd: [Criterion] = []
var selectedSearchOption: Int = 1


class SearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //OUTLETS
    
    @IBOutlet weak var cannotConnectView: UIView!
    
    @IBOutlet weak var searchTable: UITableView!
    
    
    @IBOutlet weak var searchSegmentedControl: UISegmentedControl!
    
    //VARIABLES
    
    
    let myChoicesRef = FIRDatabase.database().reference().child("Users").child((activeUser?.uid)!).child("choices")
    
    let myFactorsRef = FIRDatabase.database().reference().child("Users").child((activeUser?.uid)!).child("factors")
    
    let factorsRef = FIRDatabase.database().reference().child("Factors")
    
    let choiceCategoryRef = FIRDatabase.database().reference().child("Choices").child("ChoiceCategories")
    
   
    
    var myFactors: [Criterion] = []
    var myChoices: [Choice] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // cannotConnectView.isHidden = true
        
        searchTable.delegate = self
        searchTable.dataSource = self
        
       
        
        
        searchSegmentedControl.selectedSegmentIndex = selectedSearchOption
        
        
        preDeterminedFactors = []
        choiceCategories = []
        
        if progress == 1 {
            
            self.navigationItem.title = "Options Archive"
            
            
            searchSegmentedControl.setTitle("My options", forSegmentAt: 0)
            
            searchSegmentedControl.setTitle("Popular options", forSegmentAt: 1)
        
        
            
            
            
            myChoicesRef.observe(FIRDataEventType.childAdded, with: {(snapshot) in
            
            let myChoice = Choice(name: "")
            myChoice.name = snapshot.key
            
            self.myChoices.append(myChoice)
            
          
                
                for currentChoice in (currentDecision?.choices)! {
                    
                    if currentChoice.isSelected == true {
                        
                        for choice in self.myChoices {
                            
                            if choice.name == currentChoice.name {
                                choice.isSelected = true
                            }
                        }
                        
                    }
                }
                
            
            
            self.searchTable.reloadData()
            
       
            
            
        })
                
            
            
            choiceCategoryRef.observe(FIRDataEventType.childAdded, with: {(snapshot) in
                
                
                let choiceCategory = ChoiceCategory(name: "", choices: [])
                
                choiceCategory.name = snapshot.key
                
                choiceCategories.append(choiceCategory)
                
                
                if choiceCategories.count == 0 {
                    self.cannotConnectView.isHidden = false
                } else {
                    self.cannotConnectView.isHidden = true
                }
                
        
                
                
                
                self.searchTable.reloadData()
                
                
                
                
            })
            

            
            
            
        } else {
        
        self.navigationItem.title = "Factors Archive"
            
            searchSegmentedControl.setTitle("My factors", forSegmentAt: 0)
            
            searchSegmentedControl.setTitle("Popular factors", forSegmentAt: 1)
            
            
           
            
        myFactorsRef.observe(FIRDataEventType.childAdded, with: {(snapshot) in
        
        let myFactor = Criterion(name: "")
            myFactor.name = snapshot.key
            
            self.myFactors.append(myFactor)
            
            
            
            
            for currentFactor in (currentDecision?.criteria)! {
                
                if currentFactor.isSelected == true {
                    
                    for factor in self.myFactors {
                        
                        if factor.name == currentFactor.name {
                            factor.isSelected = true
                        }
                    }
                    
                }
            }
            
            
            
            self.searchTable.reloadData()
 
        
        })
                
            
        
        
        factorsRef.observe(FIRDataEventType.childAdded, with: {(snapshot) in
            
            
        let criteria = Criterion(name: "hey")
            
            criteria.name = snapshot.key
            
            
         
            preDeterminedFactors.append(criteria)
            
            if preDeterminedFactors.count == 0 {
                self.cannotConnectView.isHidden = false
            } else {
                self.cannotConnectView.isHidden = true
            }
            
            
            for currentFactor in (currentDecision?.criteria)! {
                
                if currentFactor.isSelected == true {
                    
                    for factor in preDeterminedFactors {
                        
                        if factor.name == currentFactor.name {
                            factor.isSelected = true
                        }
                    }
                    
                }
            }

            
            
            
            
            self.searchTable.reloadData()
        
        
        
        
        })
        
        }
        
        
        
        
        
      
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if progress == 1 && selectedSearchOption == 1{
        let sectionTitle: String = "Option categories"
        return sectionTitle
        }else {
            
            return nil
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        
        if progress == 1{
            if selectedSearchOption == 0{
                return (myChoices.count)
            }else {
            
        return (choiceCategories.count)
                
            }
        }else {
            
            if selectedSearchOption == 0{
                return (myFactors.count)
            }else {
            return (preDeterminedFactors.count)
            }
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "choiceCategoryCell") as! SearchTableViewCell
        
        
        
        
        if progress == 1 {
            
            
            if selectedSearchOption == 0{
                var myChoice = myChoices[indexPath.row]
                
                var myChoiceName = myChoice.name
                
                cell.choiceCategoryName.text! =  "\(myChoiceName)"
                
                if myChoice.isSelected == true {
                    cell.accessoryType = .checkmark
                } else {
                
                cell.accessoryType = .none
                }
                
            }else {
        
        var choiceCategory = choiceCategories[indexPath.row]
        
        var choiceCategoryName = choiceCategory.name
        
        cell.choiceCategoryName.text! =  "\(choiceCategoryName)"
        cell.accessoryType = .disclosureIndicator
            
            }
            
        } else if progress == 2 {
            
            if selectedSearchOption == 0 {
                var myFactor = myFactors[indexPath.row]
                
                var myFactorName = (myFactor.name)!
                
                cell.choiceCategoryName.text! = "\(myFactorName)"
                
                if myFactor.isSelected == true{
                    cell.accessoryType = .checkmark
                } else {
                    
                    cell.accessoryType = .none
                }
                
                
            }else {
            
            var preDeterminedFactor = preDeterminedFactors[indexPath.row]
            
            var preDeterminedFactorName = (preDeterminedFactor.name)!
            
            cell.choiceCategoryName.text! =  "\(preDeterminedFactorName)"
                
                if preDeterminedFactor.isSelected == true {
                    cell.accessoryType = .checkmark
                } else {
                    
                    cell.accessoryType = .none
                }
                
        }
        }
        
     return cell
    }
    
    
 
    @IBAction func retryButtonTapped(_ sender: Any) {
        
        self.viewDidLoad()
        
    }
    
    
    
    @IBAction func searchSegmentedControlChanged(_ sender: Any) {
        
        if searchSegmentedControl.selectedSegmentIndex == 0 {
            selectedSearchOption = 0
        }
         else if searchSegmentedControl.selectedSegmentIndex == 1{
           selectedSearchOption = 1
        }
        
       searchTable.reloadData()
        
        
    }
    
    
    

    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if progress == 1{
            
            if selectedSearchOption == 0 {
                
                let myChoiceToAdd = myChoices[indexPath.row]
                
                
                
                if myChoiceToAdd.isSelected
                    == true {
                    
                    var choiceIndex = 0
                    var choiceIndexTwo = 0
                    
                    myChoiceToAdd.isSelected = false
                    
                    
                    for choice in (currentDecision?.choices)! {
                        
                        if choice.name == myChoiceToAdd.name {
                            currentDecision?.choices.remove(at: choiceIndexTwo)
                        } else {
                            choiceIndexTwo = choiceIndexTwo + 1
                        }
                        
                       
                    }
                    
                    
                    for choice in choicesToAdd {
                        
                        if choice.name == myChoiceToAdd.name {
                            
                            choicesToAdd.remove(at: choiceIndex)
                            
                        } else {
                            choiceIndex = choiceIndex + 1
                        }
                        
                    }
                  
                    
                    
                } else {
                 myChoiceToAdd.isSelected = true
                    choicesToAdd.append(myChoiceToAdd)
                    
                }
                
                tableView.deselectRow(at: indexPath, animated: true)
                
                if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
                    if cell.accessoryType == .checkmark{
                        cell.accessoryType = .none
                    }
                    else{
                        cell.accessoryType = .checkmark
                    }
                }
            }else {
                
                let choiceCategory = choiceCategories[indexPath.row]
                
                performSegue(withIdentifier: "goToPreChoices", sender: choiceCategory)
            }
            
            
        } else if progress == 2 {
            
            
            if selectedSearchOption == 0 {
                
                let myFactorToAdd = myFactors[indexPath.row]
                
                
                
                
                if myFactorToAdd.isSelected
                    == true {
                    
                    var factorIndex = 0
                    var factorIndexTwo = 0
                    
                    myFactorToAdd.isSelected = false
                    
                    
                    
                    for factor in (currentDecision?.criteria)! {
                        
                        if factor.name == myFactorToAdd.name {
                            currentDecision?.criteria.remove(at: factorIndexTwo)
                        } else {
                            factorIndexTwo = factorIndexTwo + 1
                        }
                        
                        
                    }

                    
                    for factor in factorsToAdd {
                        
                        if factor.name == myFactorToAdd.name {
                            
                            factorsToAdd.remove(at: factorIndex)
                            
                        } else {
                            factorIndex = factorIndex + 1
                        }
                        
                    }
                    
                    
                    
                } else {
                    myFactorToAdd.isSelected = true
                factorsToAdd.append(myFactorToAdd)
                
            }
                
            }else {
            
            let factorToAdd = preDeterminedFactors[indexPath.row]

                if factorToAdd.isSelected
                    == true {
                    
                    var factorIndex = 0
                    var factorIndexTwo = 0
                    
                    factorToAdd.isSelected = false
                    
                    
                    for factor in (currentDecision?.criteria)! {
                        
                        if factor.name == factorToAdd.name {
                            currentDecision?.criteria.remove(at: factorIndexTwo)
                        } else {
                            factorIndexTwo = factorIndexTwo + 1
                        }
                        
                        
                    }
                    

                    
                    for factor in factorsToAdd {
                        
                        if factor.name == factorToAdd.name {
                            
                            factorsToAdd.remove(at: factorIndex)
                            
                        } else {
                            factorIndex = factorIndex + 1
                        }
                        
                    }
                    
                } else {
                    factorToAdd.isSelected = true
            factorsToAdd.append(factorToAdd)
                
            }
            
            }
            tableView.deselectRow(at: indexPath, animated: true)
            
            if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
                if cell.accessoryType == .checkmark{
                    cell.accessoryType = .none
                }
                else{
                    cell.accessoryType = .checkmark
                }
            }

            }
        
        NotificationCenter.default.post(name: .searchOptions, object: nil)
        
        print (choicesToAdd.count)
        
        }
    
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        if selectedSearchOption == 0 {
            return .delete
        } else {
        
        return .none
        }
    }

    
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        
        
        
        var deleteItRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.default , title: "Delete",  handler:{action, indexpath in
            print("DELETE•ACTION");
            
            
            
            let index = indexPath.row
            
            
            if progress == 1 {
                
            let choiceToRemove = self.myChoices[indexPath.row]
            
            self.myChoices.remove(at: index)
            self.myChoicesRef.child(choiceToRemove.name).removeValue()
                
            } else {
                
                 let factorToRemove = self.myFactors[indexPath.row]
                self.myFactors.remove(at: index)
                self.myFactorsRef.child(factorToRemove.name).removeValue()
                
            }
      
    
            
            self.searchTable.reloadData()
            
            
        });
        
        
        
        
        
        
        
        
        
        deleteItRowAction.backgroundColor = UIColor(colorLiteralRed: 227/255, green: 227/255, blue: 227/255, alpha: 1)
        
        return [deleteItRowAction];
        
        
    }

    
    
    
    
    
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
    if segue.identifier == "goToPreChoices" {
    
    let nextVC = segue.destination as! SearchChoicesViewController
    nextVC.choiceCategory = sender as? ChoiceCategory
    
    }
    }


}

class ChoiceCategory {
    
    var name: String
    var choices: [Choice]
    
    init(name: String, choices: [Choice]) {
        
        self.name = name
        self.choices = choices
    }
    
    
}


class PreDeterminedChoice {
    var name: String
  
    init(name: String) {
        self.name = name
       
        
}
}

    class PreDeterminedCriteria {
        
        var name: String
        
        init(name: String) {
            self.name = name
        }
        
}
