//
//  CriteriaTableViewCell.swift
//  Choyces
//
//  Created by Amanda Cosel on 5/16/17.
//  Copyright © 2017 DCApps. All rights reserved.
//

import UIKit

class CriteriaTableViewCell: UITableViewCell {
    @IBOutlet weak var grayLine: UIView!
    
    @IBOutlet weak var whiteLine: UIView!
    
    @IBOutlet weak var criteriaPriorityLabel: UILabel!
    
    @IBOutlet weak var criteriaNameLabel: UILabel!
    
    @IBOutlet weak var deleteCriteriaButton: UIButton!

    @IBOutlet weak var priorityView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
   

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
