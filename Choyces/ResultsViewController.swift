//
//  ResultsViewController.swift
//  Choyces
//
//  Created by Amanda Cosel on 5/25/17.
//  Copyright © 2017 DCApps. All rights reserved.
//

import UIKit
import ChameleonFramework
import Cheetah


class ResultsViewController: UIViewController {
    
    
    //OUTLETS
    
    @IBOutlet weak var winnerNameLabel: UILabel!
    
    @IBOutlet weak var resultsView: UIView!
    
    @IBOutlet weak var closeButton: UIButton!
   
  //  @IBOutlet weak var closeButtonView: RandomButtonView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
      
        
        let origCloseImage = UIImage(named: "Cancel-50.png")
        let tintedCloseImage = origCloseImage?.withRenderingMode(.alwaysTemplate)
        closeButton?.setImage(tintedCloseImage, for: .normal)
        closeButton?.tintColor = FlatBlack()

    //closeButtonView.mainColor = UIColor.white
       // closeButtonView.ringColor = UIColor.white
        
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        if randomWinner == true {
            
            
            
            determineRandomWinner(forChoices: (currentDecision?.choices)!)
            
        } else {
            
            //determineWinner()
            
        }
    }
    
    
    

        
    
    
    func determineRandomWinner(forChoices: [Choice]) {
        
        var random = Int(arc4random_uniform(UInt32(forChoices.count)))
        
        var currentIndex: Int = 0
        
        
        
        for choice in forChoices {
            
            
            if currentIndex == random {
                
                
                choice.isWinner = true
                winningChoice = choice
                winnerNameLabel.text = "The choYce is: \((winningChoice?.name)!)!"
                print(choice.name)
            } else {
                choice.isWinner = false
            }
            
            currentIndex = currentIndex + 1
            
        }
        
        print(random)
        
        
    }
    
    
    
    
    
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        
        
        
        if randomWinner == true {
            randomWinner = false
            
            closeButton?.cheetah
            .rotate(-M_PI).duration(0.1).run().repeatCount(3)
        
         performSegue(withIdentifier: "unwindToMainController", sender: AnyObject.self)
            
        } else {
        
            
        }
        
    }
    
    
    



}
