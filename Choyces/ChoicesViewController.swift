//
//  ViewController.swift
//  Choyces
//
//  Created by Amanda Cosel on 5/14/17.
//  Copyright © 2017 DCApps. All rights reserved.
//

import UIKit
import Firebase



class ChoicesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIPopoverPresentationControllerDelegate {
    
    
    //OUTLETS
    
    @IBOutlet weak var textInputView: UIView!

    @IBOutlet weak var choicesTable: UITableView!
    
    @IBOutlet weak var noChoicesView: UIView!
    
    @IBOutlet weak var addChoiceButton: UIButton!
    
    @IBOutlet weak var addChoiceTextField: UITextField!
    
    @IBOutlet weak var noChoicesImage: UIImageView!
    
    @IBOutlet weak var toolTipButton: UIButton!
    
    //LOCAL VARIABLES
    
    var selectedIndexPath: IndexPath? = nil
    let ref = FIRDatabase.database().reference().child("Users")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        choicesTable.delegate = self
        choicesTable.dataSource = self
       
        
        
        noChoices()

        
        let origToolTipImage = UIImage(named: "Info-50.png")
        let tintedToolTipImage = origToolTipImage?.withRenderingMode(.alwaysTemplate)
        toolTipButton?.setImage(tintedToolTipImage, for: .normal)
        toolTipButton?.tintColor = UIColor.lightGray
        
       

        
       // decisionNameLabel.text! = "\(currentDecision?.name)"
        
        
        
        
       
            }
    
    
    override func viewDidAppear(_ animated: Bool) {
        choicesTable.reloadData()
        
        //noChoices()
        addChoiceButton.isEnabled = false
        
        /*
        
        if showToolTip == true {
            
            performSegue(withIdentifier: "goToTip", sender: AnyObject.self)
            
        } else {
            
        }
 
 */
        
        
            }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (currentDecision?.choices.count)!
    }
    
    func noChoices() {
        if (currentDecision?.choices.count)! > 0 {
            noChoicesView.isHidden = true
            
        } else {
            noChoicesImage?.image = noChoicesImage?.image!.withRenderingMode(.alwaysTemplate)
            noChoicesImage?.tintColor = UIColor.lightGray
            noChoicesView.isHidden = false
            
        }
        
       

        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "choiceCell") as! ChoicesTableViewCell
        var choice = currentDecision?.choices[indexPath.row]
        
        var choiceName = (choice?.name)!
        
        cell.choiceNameLabel.text! =  "\(choiceName)"
        
        
        // cell.deleteChoiceButton.addTarget(self,action:#selector(deleteChoice(sender:)), for: .touchUpInside)
        
        if indexPath.row == ((currentDecision?.choices.count)! - 1) {
            
            
            
            cell.grayLine.isHidden = true
            
        } else {
            cell.grayLine.isHidden = false
           
            
        }


        
        return cell
    }
    
    
    func deleteChoice(sender:UIButton) {
        
        let buttonRow = sender.tag
        
        currentDecision?.choices.remove(at: buttonRow)
        
       // self.viewDidAppear(true)
       // self.viewDidLoad()
        
        
        
        choicesTable.reloadData()
        
        
        choicesProgress = choicesProgress - 0.1
        NotificationCenter.default.post(name: .reload, object: nil)
        noChoices()
        
   
    }

    
    
    @IBAction func addChoiceButtonTapped(_ sender: Any) {
        
        
        
        var newChoice = Choice(name: "\(addChoiceTextField.text!)")
        
        newChoice.isSelected = true
        
        
        let newChoiceRef = self.ref.child((activeUser?.uid)!).child("choices").child((addChoiceTextField.text!.lowercased()))
        
        newChoiceRef.setValue(newChoice.toAnyObject())
        
        
        
        currentDecision?.choices.append(newChoice)
        
       // self.viewDidAppear(true)
        //self.viewDidLoad()
        
        choicesTable.reloadData()
        
        choicesProgress = choicesProgress + 0.1
        
        addChoiceTextField.text = ""
        addChoiceButton.isEnabled = false
        NotificationCenter.default.post(name: .reload, object: nil)
        noChoices()
        
    }
    
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        
        
    }
    
    
    
    
 
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        var newChoice = Choice(name: "\(addChoiceTextField.text)")
        
        currentDecision?.choices.append(newChoice)
        
        choicesTable.reloadData()
      
        
        addChoiceTextField.resignFirstResponder()
        return (true)
        
    }
    
    
    @IBAction func choiceTextChanged(_ sender: Any) {
        
        if addChoiceTextField.text == "" {
            
            addChoiceButton.isEnabled = false
            
        } else {
            
            addChoiceButton.isEnabled = true
            
        }
    }
    
    
    /*
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToTip" {
            
            let popoverViewController = segue.destination
            popoverViewController.popoverPresentationController?.delegate = self
            let vc = segue.destination as? ToolTIpViewController
            let pop = vc?.popoverPresentationController
            pop?.delegate = vc as UIPopoverPresentationControllerDelegate?
            
            }

    }
    
*/
    
    
    /*
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
        switch(selectedIndexPath)
        {
            
        case nil:
            
            let cell = choicesTable.cellForRow(at: indexPath) as! ChoicesTableViewCell
           // cell.arrowImagem.image = UIImage(named:"content_arrow2")
            selectedIndexPath = indexPath
            cell.backgroundColor = UIColor.white
            
            
            
        default:
            if selectedIndexPath == indexPath
            {
                
                let cell = choicesTable.cellForRow(at: indexPath) as! ChoicesTableViewCell
                //cell.arrowImagem.image = UIImage(named:"content_arrow")
                //cell.reloadInputViews()
                selectedIndexPath = nil
                cell.backgroundColor = UIColor.white
                
            }
            
            
        }
        
        //self.myTableView.beginUpdates()
        
        self.choicesTable.reloadData()
        
        
        //myTableView.reloadRows(at: [indexPath], with: .automatic)
        
        self.choicesTable.endUpdates()
        
    }
    
    
    */
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        
        
        
        var deleteItRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.default , title: "Delete",  handler:{action, indexpath in
            print("DELETE•ACTION");
            
            
            
            let index = indexPath.row
            currentDecision?.choices.remove(at: index)
            
            //self.viewDidAppear(true)
            //self.viewDidLoad()
            
            self.choicesTable.reloadData()
            choicesProgress = choicesProgress - 0.1
            NotificationCenter.default.post(name: .reload, object: nil)
            self.noChoices()
            
         
            
            
        
          
        });
        
        
        
        
        
        
        
        
        
        deleteItRowAction.backgroundColor = UIColor(colorLiteralRed: 227/255, green: 227/255, blue: 227/255, alpha: 1)
      
            return [deleteItRowAction];
        
    }

    
    
    
    
    
    /*
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        
        
        let index = indexPath
        
        if selectedIndexPath != nil{
            if(index == selectedIndexPath)
            {
                
                return 160
            }
            else{
                return 70
            }
        }
        else
        {
            return 70
        }
        
        
        
    }
    
    */
    
}

/*
struct Criterion {
    
    var name : String
    var priority : Int
    var relatedChoices : [Choice]
    
}

struct Choice {
    
    var name : String
    var criteriaChoiceRankings : [Ranking]
    var relatedCriteria : [Criterion]
    var overallScore : Int
    
    
}

struct Ranking {
    var rankOfChoiceInCriteria : Int
    var overallRank : Int
    var relatedChoice : Choice
    var relatedCriteria : Criterion
    
}
 */


class Decision {
    
    var name: String
    var choices: [Choice] = []
    var criteria: [Criterion] = []
    var relatedChoices: [Choice] = []
    
    init(name: String) {
        
        self.name = name
    }
    
    
    func rankChoiceForCriteria ( firstChoice: IndexPath, lastChoice: IndexPath) {
        
        for index in firstChoice.row...lastChoice.row {
            //            choiceClass?.choices[index].priority = index + 1
            
            var rankOfChoiceInCriteria = choices.count - (index)
            
            var newRanking = rankOfChoiceInCriteria * (criteria[currentCriteriaIndex].priority)
            
            choices[index].rankings.append(newRanking)
            
        }
        
        
        
        
        
    }
    
    
    func removeRankChoiceForCriteria(firstChoice: IndexPath, lastChoice: IndexPath) {
        
        for index in firstChoice.row...lastChoice.row {
            currentDecision?.choices[index].rankings.remove(at: currentCriteriaIndex - 1)
            
        }
        
    }


    
    
}




class Choice {
    var name: String
    var rankings: [Int] = []
    var overallScore: Int = 0
    var isWinner: Bool = false
    var relatedCriteria: [Criterion] = []
    var isSelected: Bool = false
    
    init(name: String) {
        self.name = name
      
    }
    
    func toAnyObject() -> [AnyHashable:Any] {
        return ["name":name] as [AnyHashable:Any]
    }

    
    func determineOverallScore() {
        
        var score: Int = 0
        
        for ranking in rankings {
            
            score = score + ranking
            
        }
        
       overallScore = score
        print("\(name): \(overallScore)")
        
    }
    
    


}

class Criterion {
    var name: String!
    var priority: Int = 0
    var isSelected: Bool = false
    
  
    
    init(name: String){
        self.name = name
    }
    
    func toAnyObject() -> [AnyHashable:Any] {
        return ["name":name, "priority":priority] as [AnyHashable:Any]
    }
    
    
    
}




class User {
    
    var email: String = ""
    var uid: String = ""
    var name: String = ""
    var pic: 
    
    init(uid: String){
        self.uid = uid
    }

    
}






