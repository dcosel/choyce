//
//  SearchChoicesTableViewCell.swift
//  Choyces
//
//  Created by Amanda Cosel on 6/4/17.
//  Copyright © 2017 DCApps. All rights reserved.
//

import UIKit

class SearchChoicesTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var searchChoicesNameLabel: UILabel!
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
