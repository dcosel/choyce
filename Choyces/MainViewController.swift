//
//  MainViewController.swift
//  Choyces
//
//  Created by Amanda Cosel on 5/26/17.
//  Copyright © 2017 DCApps. All rights reserved.
//

import UIKit
import ChameleonFramework
import BubbleTransition
import Cheetah

//import SingleLineShakeAnimation



//GLOBAL VARIABLES

var decisions: [Decision] = []
var currentDecision: Decision? = nil
var progress: Int = 0
var choicesProgress: Float = 0.0
var factorsProgress: Float = 0.0
var rankingsProgress: Float = 0.0
var winningChoice: Choice? = nil
var randomWinner: Bool = false
var currentCriteriaIndex : Int = 0
var finishedLoading:Int = 1
var controllers = [UIViewController]()
var current: Int = 1
var previous: Int? = nil
var choiceCategories: [ChoiceCategory] = []
var preDeterminedFactors: [Criterion] = []
var hasLaunched: Bool = true
var showToolTip: Bool = true
var helpPageIndex: Int = 0
var usePreLoadedFactors: Bool? = nil




class MainViewController: UIViewController, UIPopoverPresentationControllerDelegate, UIViewControllerTransitioningDelegate {
    
    //OUTLETS
   
    @IBOutlet weak var upNextTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var decisionTitleLabel: UILabel!
    @IBOutlet weak var upNextLabel: UILabel!
    @IBOutlet weak var upNextView: UIView!
    @IBOutlet weak var circleView: RandomButtonView!
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var decisionCircleView: RandomButtonView?
    
    @IBOutlet weak var decisionCheckMarkImage: UIImageView?
    
   // @IBOutlet weak var currentPageLabel: UILabel?
    
    @IBOutlet weak var progressBar: UIProgressView?
    
    @IBOutlet weak var factorsProgressBar: UIProgressView?
    
    
    @IBOutlet weak var rankingsProgressBar: UIProgressView?
    
    
    @IBOutlet weak var choicesImage: UIImageView?
    @IBOutlet weak var choicesButton: UIButton!
    
    @IBOutlet weak var rankingsImage: UIImageView?
    @IBOutlet weak var choicesSelector: TriangleView?
    
    @IBOutlet weak var factorsImage: UIImageView?
    
    @IBOutlet weak var factorsSelector: TriangleView?
    
    @IBOutlet weak var rankingsSelector: TriangleView?
    
    @IBOutlet weak var factorButton: UIButton?
    
    @IBOutlet weak var rankingButton: UIButton?
    
    @IBOutlet weak var randomButtonView: RandomButtonView?
   
    @IBOutlet weak var randomButton: UIButton?
    
    
    @IBOutlet weak var statusView: UIView?
    
    
    @IBOutlet weak var rightArrow: UIImageView?
    
   // @IBOutlet weak var leftArrow: UIImageView?
    
    @IBOutlet weak var upNextButton: UIImageView?
    
    //@IBOutlet weak var previousButton: UIButton?
    
    @IBOutlet weak var maskView: UIView!
    
    
    
    //LOCAL VARIABLES
    
    var containerView: ContainerViewController?
    var yesNo: Bool = false
    let transition = BubbleTransition()
    
    
    
    
   // let ref = FIRDatabase.database().reference(withPath: "Decisions")

    override func viewDidLoad() {
        super.viewDidLoad()
        
        maskView.isHidden = true
        
        let origmenuImage = UIImage(named: "Menu Filled-50.png")
        let tintedMenuImage = origmenuImage?.withRenderingMode(.alwaysTemplate)
        menuButton?.setImage(tintedMenuImage, for: .normal)
        menuButton?.tintColor = UIColor.white
        
        
       //goToChoices()
        
        determineProgress()
       determineButtonStatesTwo()
        
        
      decisionTitleLabel.text! = "\((currentDecision?.name)!)"
        
        decisionTitleLabel.textColor = UIColor.white
        //
        
        
        let origSearchImage = UIImage(named: "Folder-50.png")
        let tintedSearchImage = origSearchImage?.withRenderingMode(.alwaysTemplate)
        searchButton?.setImage(tintedSearchImage, for: .normal)
        searchButton?.tintColor = UIColor.white
        
        
        let origInfoImage = UIImage(named: "Info-50.png")
        let tintedInfoImage = origInfoImage?.withRenderingMode(.alwaysTemplate)
        infoButton?.setImage(tintedInfoImage, for: .normal)
        infoButton?.tintColor = UIColor.white
        
      
        choicesSelector?.isHidden = false
        factorsSelector?.isHidden = true
        rankingsSelector?.isHidden = true
        
      
        
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(determineButtonStates(_:)), name: .reload , object: nil)
    
        NotificationCenter.default.addObserver(self, selector: #selector(determineProgressOfSwipes(_:)), name: .pageSwiped , object: nil)
       
        
        
        
        
      
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        decisionCircleView?.mainColor = UIColor.white
        randomButtonView?.mainColor = FlatBlack()
        showToolTipView()
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    

    func showToolTipView() {
        
        if (showToolTip == true) && (progress == 1 || progress == 2) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
            
            
         
            
            
            
            
        self.performSegue(withIdentifier: "goToTip", sender: AnyObject.self)
            }
        }
        
    }
    
    /*
    
    @IBAction func previousButtonTapped(_ sender: Any) {
        
        progress = progress - 1
        determineProgress()
        print(progress)
        
    }
 
 */
    
    @IBAction func upNextButtontapped(_ sender: Any) {
        
      
        
       
            progress = progress + 1
            determineProgress()
        showToolTipView()
            
    
        
        }
    
    
    
    
    func determineProgress() {
        
        decisionCheckMarkImage?.image = decisionCheckMarkImage?.image!.withRenderingMode(.alwaysTemplate)
        decisionCheckMarkImage?.tintColor = UIColor.green
        
        decisionCircleView?.mainColor = UIColor.white
        decisionCircleView?.ringColor = UIColor.white
       
        
        if progress == 1 {
            goToChoices()
           
            progressBar?.progress = choicesProgress
            factorsProgressBar?.progress = 0.0
            rankingsProgressBar?.progress = 0.0
            //currentPageLabel?.text = "Add Options"
            //previousButton?.isEnabled = false
            searchButton.isHidden = false
            if (currentDecision?.choices.count)! > 0 {
                
           // upNextView.isHidden = false
                
            displayNextUpView()
            upNextLabel.text = "Up next: Add Factors"
            //rightArrow?.isHidden = false
            }else {
                hideNextUpView()
               // upNextView.isHidden = true
               // rightArrow?.isHidden = true
            }
          //  leftArrow?.isHidden = true
            
        }
        
        else if progress == 2 {
          
            goToFactors()
            progressBar?.progress = 1.0
            factorsProgressBar?.progress = factorsProgress
            rankingsProgressBar?.progress = 0.0
            //currentPageLabel?.text = "Add Factors"
           // previousButton?.isEnabled = true
            searchButton.isHidden = false
            if (currentDecision?.criteria.count)! > 0 {
                //upNextView.isHidden = false
                 displayNextUpView()
                upNextLabel.text = "Up next: Prioritize Factors"
           // rightArrow?.isHidden = false
            } else {
                hideNextUpView()
                
               // upNextView.isHidden = true
                //rightArrow?.isHidden = true
            }
            //leftArrow?.isHidden = false
        }
        
        
        else if progress == 3 {
            goToFactors()
          
            progressBar?.progress = 1.0
            factorsProgressBar?.progress = 0.6
            rankingsProgressBar?.progress = 0.0
           // currentPageLabel?.text = "Prioritize Factors"
             displayNextUpView()
            //upNextView.isHidden = false
            upNextLabel.text = "Up next: Rankings"
            //rightArrow?.isHidden = false
           // leftArrow?.isHidden = false
            searchButton.isHidden = true
            
        }else if progress == 4 {
            
            goToRankings()
           
            progressBar?.progress = 1.0
            factorsProgressBar?.progress = 1.0
            rankingsProgressBar?.progress = rankingsProgress
            
            
            
            
            //currentPageLabel?.text = "Rankings"
           // upNextView.isHidden = false
             displayNextUpView()
            upNextLabel.text = "Swipe to next choice"
            //rightArrow?.isHidden = true
            //leftArrow?.isHidden = false
            searchButton.isHidden = true
            
        } else {}
        
        
        
    }
    
    func determineProgressOfSwipes(_ notification: Notification) {
        
        
        rankingsProgressBar?.progress = rankingsProgress
        
        if rankingsProgress == 1.0 {
            
            decisionCheckMarkImage?.image = decisionCheckMarkImage?.image!.withRenderingMode(.alwaysTemplate)
            decisionCheckMarkImage?.tintColor = UIColor.white
            
            decisionCircleView?.mainColor = UIColor.green
            decisionCircleView?.ringColor = UIColor.green
            
            hideNextUpView()
            
            
            
           // currentPageLabel?.text = "Results"
           // rightArrow?.isHidden = true
            

            
        } else {
            
            decisionCheckMarkImage?.image = decisionCheckMarkImage?.image!.withRenderingMode(.alwaysTemplate)
            decisionCheckMarkImage?.tintColor = UIColor.green
            
            decisionCircleView?.mainColor = UIColor.white
            decisionCircleView?.ringColor = UIColor.white
            
            displayNextUpView()

            //currentPageLabel?.text = "Rankings"
            //rightArrow?.isHidden = false
            
            
            
        }
    }
    
    
    func hideNextUpView() {
        
        self.view.layoutIfNeeded()
        //self.view.layoutIfNeeded()
        
        
        
        UIView.animate(withDuration: Double(0.3), animations: {
            self.upNextTrailingConstraint.constant = -260
            self.view.layoutIfNeeded()
        })

    }
        
        
    func displayNextUpView() {
        
       // self.centerYConstraint.constant = 500.0
        self.view.layoutIfNeeded()
        //self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: Double(0.8), animations: {
            self.upNextTrailingConstraint.constant = 0
            
            self.view.layoutIfNeeded()
        })

}
    

  
    
    @IBAction func choicesButtonTapped(_ sender: Any) {
        
        
        
        
        progress = 1
        goToChoices()
        determineProgress()
     
        
    }
    
    
    func goToChoices() {
        
        containerView!.segueIdentifierReceivedFromParent(button: "choices")
        choicesSelector?.isHidden = false
        factorsSelector?.isHidden = true
        rankingsSelector?.isHidden = true
        resetRankings()
        factorButton?.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightHeavy)
        
        rankingButton?.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightHeavy)
        //rankingButton?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        choicesButton?.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightHeavy)
        
        
        choicesImage?.image = UIImage(named: "Checked Checkbox Filled-50.png")
        factorsImage?.image = UIImage(named: "icons8-Up Squared-50.png")
        rankingsImage?.image = UIImage(named: "icons8-1-50.png")
        
        
        choicesImage?.image = choicesImage?.image!.withRenderingMode(.alwaysTemplate)
        
       // choicesImage?.image = choicesImage?.highlightedImage?.withRenderingMode(.alwaysTemplate)
         factorsImage?.image = factorsImage?.image!.withRenderingMode(.alwaysTemplate)
        rankingsImage?.image = rankingsImage?.image!.withRenderingMode(.alwaysTemplate)
        
        
    }
    
    
    
    @IBAction func factorsButtonTapped(_ sender: Any) {
        progress = 2
        determineProgress()
        
    }
    
    
    func goToFactors() {
        
        
        containerView?.segueIdentifierReceivedFromParent(button: "factors")
        choicesSelector?.isHidden = true
        factorsSelector?.isHidden = false
        rankingsSelector?.isHidden = true
        factorButton?.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightHeavy)
        
        rankingButton?.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightHeavy)
        //rankingButton?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        choicesButton?.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightHeavy)
        
        resetRankings()
        
        choicesImage?.image = UIImage(named: "icons8-Checked Checkbox-50.png")
        factorsImage?.image = UIImage(named: "icons8-Up Squared Filled-50.png")
        rankingsImage?.image = UIImage(named: "icons8-1-50.png")
        
       
        factorsImage?.image = factorsImage?.image!.withRenderingMode(.alwaysTemplate)
        
        
        choicesImage?.image = choicesImage?.image!.withRenderingMode(.alwaysTemplate)
     rankingsImage?.image = rankingsImage?.image!.withRenderingMode(.alwaysTemplate)
        
        
    }
    
    
    
    @IBAction func RankingsButtonTapped(_ sender: Any) {
        
        progress = 4
        //rankingsProgress = 0.0
        determineProgress()
        
    }
    
    func goToRankings() {
        
        choicesSelector?.isHidden = true
        factorsSelector?.isHidden = true
        rankingsSelector?.isHidden = false
        
        controllers = []
        populateControllersArray(forCriteria: currentDecision!.criteria)
        containerView?.segueIdentifierReceivedFromParent(button: "ranking")
        currentCriteriaIndex = 0
        rankingsProgress = 0.0
        factorButton?.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightHeavy)
        
        rankingButton?.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightHeavy)
        //rankingButton?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        choicesButton?.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightHeavy)
        
        
        choicesImage?.image = UIImage(named: "icons8-Checked Checkbox-50.png")
        factorsImage?.image = UIImage(named: "icons8-Up Squared-50.png")
        rankingsImage?.image = UIImage(named: "icons8-1 Filled-50.png")
        
        choicesImage?.image = choicesImage?.image!.withRenderingMode(.alwaysTemplate)
        factorsImage?.image = factorsImage?.image!.withRenderingMode(.alwaysTemplate)
        rankingsImage?.image = rankingsImage?.image!.withRenderingMode(.alwaysTemplate)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "container"{
            containerView = segue.destination as? ContainerViewController
            
        }
        
//        if segue.identifier == "goToRandomResult" {
//        let controller = segue.destination
//        controller.transitioningDelegate = self
//       controller.modalPresentationStyle = .custom
//        
//        
//        }
       /*
        if segue.identifier == "goToSearch" {
            
            maskView.isHidden = false
            let origSearchImage = UIImage(named: "Search Filled-50.png")
            let tintedSearchImage = origSearchImage?.withRenderingMode(.alwaysTemplate)
            searchButton?.setImage(tintedSearchImage, for: .normal)
            searchButton?.tintColor = UIColor.white

            
        }
 
 */
        
        
        if segue.identifier == "goToTip" {
            
            let popoverViewController = segue.destination
            popoverViewController.popoverPresentationController?.delegate = self
            let vc = segue.destination as? ToolTIpViewController
            let pop = vc?.popoverPresentationController
            pop?.delegate = vc as UIPopoverPresentationControllerDelegate?
            
        }


    }
    
    
//    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        transition.transitionMode = .present
//        transition.startingPoint = (circleView?.center)!
//        transition.bubbleColor = FlatBlack()
//        return transition
//    }
//    
//    
//    
//    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        transition.transitionMode = .dismiss
//        transition.startingPoint = (circleView?.center)!
//        transition.bubbleColor = UIColor.white
//        return transition
//    }
    
    
    func determineButtonStates(_ notification: Notification) {

        
        determineProgress()
        
        if (currentDecision?.choices.count)! > 0 && (currentDecision?.criteria.count)! > 0 {
            
           
            factorButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
            rankingButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
            
            factorButton?.isEnabled = true
            
            rankingButton?.isEnabled = true
            //statusView?.isHidden = false
            
            //randomButtonView?.isHidden = false
           // randomButton?.isHidden = false
            //rankingsImage?.image = rankingsImage?.image!.withRenderingMode(.alwaysTemplate)
            rankingsImage?.tintColor = UIColor.white
           // factorsImage?.image = factorsImage?.image!.withRenderingMode(.alwaysTemplate)
            factorsImage?.tintColor = UIColor.white
            
            
            
            
         
           // choicesImage?.highlightedImage = choicesImage?.highlightedImage.withRenderingMode(.alwaysTemplate)
            
           // choicesImage?.image = choicesImage?.image!.withRenderingMode(.alwaysTemplate)
            choicesImage?.tintColor = UIColor.white
            
            let origRandomImage = UIImage(named: "Dice-50.png")
            let tintedRandomImage = origRandomImage?.withRenderingMode(.alwaysTemplate)
            randomButton?.setImage(tintedRandomImage, for: .normal)
            randomButton?.tintColor = UIColor.white
            randomButton?.isEnabled = true
            circleView?.ringColor = UIColor.white
            
        } else if (currentDecision?.choices.count)! > 0 && currentDecision?.criteria.count == 0 {
            
            
            
            factorButton?.setTitleColor(UIColor(colorLiteralRed: 255/255, green: 255/255, blue:
                255/255, alpha: 1), for: UIControlState.normal)
            rankingButton?.setTitleColor(UIColor(colorLiteralRed: 255/255, green: 255/255, blue:
                255/255, alpha: 0.3), for: UIControlState.normal)
            
            factorButton?.isEnabled = true
          
            rankingButton?.isEnabled = false
          // statusView?.isHidden = false
           // randomButtonView?.isHidden = false
            //randomButton?.isHidden = false
            //rankingsImage?.image = rankingsImage?.image!.withRenderingMode(.alwaysTemplate)
            rankingsImage?.tintColor = UIColor(colorLiteralRed: 255/255, green: 255/255, blue:
                255/255, alpha: 0.3)
            //factorsImage?.image = factorsImage?.image!.withRenderingMode(.alwaysTemplate)
            factorsImage?.tintColor = UIColor.white
           // choicesImage?.image = choicesImage?.image!.withRenderingMode(.alwaysTemplate)
            choicesImage?.tintColor = UIColor.white
            
            
            let origRandomImage = UIImage(named: "Dice-50.png")
            let tintedRandomImage = origRandomImage?.withRenderingMode(.alwaysTemplate)
            randomButton?.setImage(tintedRandomImage, for: .normal)
            randomButton?.tintColor = UIColor.white
            randomButton?.isEnabled = true
            circleView?.ringColor = UIColor.white
            
        } else {
            
            factorButton?.setTitleColor(UIColor(colorLiteralRed: 255/255, green: 255/255, blue:
                255/255, alpha: 0.3), for: UIControlState.normal)
            rankingButton?.setTitleColor(UIColor(colorLiteralRed: 255/255, green: 255/255, blue:
                255/255, alpha: 0.3), for: UIControlState.normal)
            
            factorButton?.isEnabled = false
    
            rankingButton?.isEnabled = false
           //statusView?.isHidden = true
           // randomButtonView?.isHidden = true
           // randomButton?.isHidden = true
            //choicesImage?.image = choicesImage?.image!.withRenderingMode(.alwaysTemplate)
            choicesImage?.tintColor = UIColor.white
            //factorsImage?.image = factorsImage?.image!.withRenderingMode(.alwaysTemplate)
            factorsImage?.tintColor = UIColor(colorLiteralRed: 255/255, green: 255/255, blue:
                255/255, alpha: 0.3)
            //rankingsImage?.image = rankingsImage?.image!.withRenderingMode(.alwaysTemplate)
            rankingsImage?.tintColor = UIColor(colorLiteralRed: 255/255, green: 255/255, blue:
                255/255, alpha: 0.3)
            
            let origRandomImage = UIImage(named: "Dice-50.png")
            let tintedRandomImage = origRandomImage?.withRenderingMode(.alwaysTemplate)
            randomButton?.setImage(tintedRandomImage, for: .normal)
            randomButton?.tintColor = UIColor.darkGray
            randomButton?.isEnabled = false
            circleView?.ringColor = UIColor.darkGray
            
            
        }
        
        
    }
    
    func resetRankings() {
        currentCriteriaIndex = 0
        
        for choice in (currentDecision?.choices)! {
            
            choice.overallScore = 0
            choice.rankings = []
        }
        
        
    }
    
    
    func populateControllersArray(forCriteria: [Criterion]) {
        
       // var criteriaIndex: Int = 0
        
        for criteria in forCriteria {
            let controller = storyboard?.instantiateViewController(withIdentifier: "rankingViewController") as! UIViewController
            //controller.evaluatedCriteriaName.text = currentDecision?.criteria[criteriaIndex].name
           
            // controller.index = criteria
            controllers.append(controller)
           
            print(controllers.count)
            //criteriaIndex = criteriaIndex + 1
        }
        
        let resultsController = storyboard?.instantiateViewController(withIdentifier: "calculatedResults") as! UIViewController
        
        controllers.append(resultsController)
    
    }
    
    
    func determineButtonStatesTwo() {
         determineProgress()
        
        if (currentDecision?.choices.count)! > 0 && (currentDecision?.criteria.count)! > 0 {
            
            
            factorButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
            rankingButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
            factorButton?.isEnabled = true
            rankingButton?.isEnabled = true
            //statusView?.isHidden = false
            //randomButtonView?.isHidden = false
            //randomButton?.isHidden = false
            //rankingsImage?.image = rankingsImage?.image!.withRenderingMode(.alwaysTemplate)
            rankingsImage?.tintColor = UIColor.white
            //factorsImage?.image = factorsImage?.image!.withRenderingMode(.alwaysTemplate)
            factorsImage?.tintColor = UIColor.white
            //choicesImage?.image = choicesImage?.image!.withRenderingMode(.alwaysTemplate)
            choicesImage?.tintColor = UIColor.white
            
            let origRandomImage = UIImage(named: "Dice-50.png")
            let tintedRandomImage = origRandomImage?.withRenderingMode(.alwaysTemplate)
            randomButton?.setImage(tintedRandomImage, for: .normal)
            randomButton?.tintColor = UIColor.white
            randomButton?.isEnabled = true
            circleView?.ringColor = UIColor.white
            
        } else if (currentDecision?.choices.count)! > 0 && currentDecision?.criteria.count == 0 {
            
            factorButton?.isEnabled = true
           
            factorButton?.setTitleColor(UIColor(colorLiteralRed: 255/255, green: 255/255, blue:
                255/255, alpha: 0.3), for: UIControlState.normal)
            rankingButton?.setTitleColor(UIColor(colorLiteralRed: 255/255, green: 255/255, blue:
                255/255, alpha: 0.3), for: UIControlState.normal)
            rankingButton?.isEnabled = false
            //statusView?.isHidden = false
            //randomButtonView?.isHidden = false
            //randomButton?.isHidden = false
            //rankingsImage?.image = rankingsImage?.image!.withRenderingMode(.alwaysTemplate)
            rankingsImage?.tintColor = UIColor(colorLiteralRed: 255/255, green: 255/255, blue:
                255/255, alpha: 0.3)
            //factorsImage?.image = factorsImage?.image!.withRenderingMode(.alwaysTemplate)
            factorsImage?.tintColor = UIColor(colorLiteralRed: 255/255, green: 255/255, blue:
                255/255, alpha: 0.3)
            
           // choicesImage?.image = choicesImage?.image!.withRenderingMode(.alwaysTemplate)
            choicesImage?.tintColor = UIColor.white
            
            let origRandomImage = UIImage(named: "Dice-50.png")
            let tintedRandomImage = origRandomImage?.withRenderingMode(.alwaysTemplate)
            randomButton?.setImage(tintedRandomImage, for: .normal)
            randomButton?.tintColor = UIColor.white
            randomButton?.isEnabled = true
            circleView?.ringColor = UIColor.white
        } else {
            
          
            
            factorButton?.setTitleColor(UIColor(colorLiteralRed: 255/255, green: 255/255, blue:
                255/255, alpha: 0.3), for: UIControlState.normal)
            rankingButton?.setTitleColor(UIColor(colorLiteralRed: 255/255, green: 255/255, blue:
                255/255, alpha: 0.3), for: UIControlState.normal)
            
            factorButton?.isEnabled = false
            
            rankingButton?.isEnabled = false
            //statusView?.isHidden = true
            //randomButtonView?.isHidden = true
            //randomButton?.isHidden = true
           // choicesImage?.image = choicesImage?.image!.withRenderingMode(.alwaysTemplate)
            choicesImage?.tintColor = UIColor.white
           // factorsImage?.image = factorsImage?.image!.withRenderingMode(.alwaysTemplate)
            factorsImage?.tintColor = UIColor(colorLiteralRed: 255/255, green: 255/255, blue:
                255/255, alpha: 0.3)
            //rankingsImage?.image = rankingsImage?.image!.withRenderingMode(.alwaysTemplate)
            rankingsImage?.tintColor = UIColor(colorLiteralRed: 255/255, green: 255/255, blue:
                255/255, alpha: 0.3)
            
            let origRandomImage = UIImage(named: "Dice-50.png")
            let tintedRandomImage = origRandomImage?.withRenderingMode(.alwaysTemplate)
            randomButton?.setImage(tintedRandomImage, for: .normal)
            randomButton?.tintColor = UIColor.darkGray
            randomButton?.isEnabled = false
            circleView?.ringColor = UIColor.darkGray
            
        }
        
        
    }
    
    
    
    

    
    
     @IBAction func unwindToViewPopular(segue: UIStoryboardSegue) {
        
        
        
        
        
        
        if yesNo == true {
            
            currentDecision?.choices = []
            let newChoiceYes = Choice(name: "Yes")
            let newChoiceNo = Choice(name: "No")
            currentDecision?.choices.append(newChoiceYes)
            currentDecision?.choices.append(newChoiceNo)
            choicesProgress = 0.2
            determineButtonStatesTwo()
        
        
        }else if usePreLoadedFactors == false {
        determineButtonStatesTwo()
            usePreLoadedFactors = nil
        
        
        }else {
        
        
        DispatchQueue.main.async {
        
    
                
                self.maskView.isHidden = false
                let origSearchImage = UIImage(named: "Folder-50.png")
                let tintedSearchImage = origSearchImage?.withRenderingMode(.alwaysTemplate)
                self.searchButton?.setImage(tintedSearchImage, for: .normal)
                self.searchButton?.tintColor = UIColor.white
                
                
                
                self.performSegue(withIdentifier: "goToSearch", sender: self)
            
                
            

            
            
            
        }
        
        }
       
           }
    
    
    @IBAction func unwindToSignOut(segue: UIStoryboardSegue) {
        
        activeUser = nil
        currentDecision = nil
        
        DispatchQueue.main.async {
            
            self.performSegue(withIdentifier: "showSignInPage", sender: self)
            
        }
        
    }
   

    
    
    
    @IBAction func unwindToMainController(segue: UIStoryboardSegue) {
        
        maskView.isHidden = true
        let origInfoImage = UIImage(named: "Info-50.png")
        let tintedInfoImage = origInfoImage?.withRenderingMode(.alwaysTemplate)
        infoButton?.setImage(tintedInfoImage, for: .normal)
        infoButton?.tintColor = UIColor.white
        let origSearchImage = UIImage(named: "Folder-50.png")
        let tintedSearchImage = origSearchImage?.withRenderingMode(.alwaysTemplate)
        searchButton?.setImage(tintedSearchImage, for: .normal)
        searchButton?.tintColor = UIColor.white
        
        
        if factorsToAdd == nil {
            
        }else {
            for factor in factorsToAdd {
                
                
                currentDecision?.criteria.append(factor)
                factorsProgress = factorsProgress + 0.05
                
            }
            
            self.viewDidLoad()
            
            
        }
        
        if choicesToAdd == nil {
            print("do nothing")
        }else {
            
            for choice in choicesToAdd {
                
               
                currentDecision?.choices.append(choice)
                choicesProgress = choicesProgress + 0.1
                
            }
            
            self.viewDidLoad()
            
        }
        
        choicesToAdd = []
        factorsToAdd = []

    }
    
    
    
    @IBAction func randomButtonTapped(_ sender: Any) {
        
       
        randomButton?.cheetah
            .move(5, 0).duration(0.1)
            
            //.rotate(-M_PI).duration(0.2)
            .wait()
            .move(-10, 0).duration(0.1)
      
            .wait()
            .move(5, -5).duration(0.1)
            
       
            .wait()
            .move(0, 10).duration(0.1)
            
           
            .wait()
            .move(0, -5).duration(0.1)
            .wait()
            .move(5, 0).duration(0.1)
            
           
            .wait()
            .move(-10, 0).duration(0.1)
            
            
            .wait()
            .move(5, -5).duration(0.1)
         
            .wait()
            .move(0, 10).duration(0.1)
           
            .wait()
            .move(0, -5).duration(0.1)
            
           
            .run()
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.9) {
            
            
            
            self.statusView?.cheetah
            .move(0, -200).duration(0.1)
            .run()
            
            randomWinner = true
            self.performSegue(withIdentifier: "goToRandomResult", sender: AnyObject.self)
        }

     
        
        
    }
    
    
    
    
    @IBAction func searchButtonTapped(_ sender: Any) {
        
                maskView.isHidden = false
        
                performSegue(withIdentifier: "goToSearch", sender: AnyObject.self)
        
    }
    
    
    
    
    /*
    
    @IBAction func helpButtonTapped(_ sender: Any) {
        
        maskView.isHidden = false
        let origInfoImage = UIImage(named: "Help Filled-50.png")
        let tintedInfoImage = origInfoImage?.withRenderingMode(.alwaysTemplate)
        infoButton?.setImage(tintedInfoImage, for: .normal)
        infoButton?.tintColor = UIColor.white
        performSegue(withIdentifier: "goToHelp", sender: AnyObject.self)
    
    }
 */
 
    
    
//    func createAlert(title: String, message: String) {
//        
//        
//        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        
//        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler: { (action) in
//            
//            alert.dismiss(animated: true, completion: nil)
//            
//            
//        }))
//        
//        alert.addAction(UIAlertAction(title: "Proceed", style: .default, handler: { (action) in
//            
//            
//            
//            alert.dismiss(animated: true, completion: nil)
//            
//            
//            
//            
//            var newDecision = Decision(name: "")
//            
//            currentDecision = newDecision
//            decisions.append(newDecision)
//            
//            self.performSegue(withIdentifier: "unwindToDecision", sender: AnyObject.self)
//            usePreLoadedFactors = nil
//            self.yesNo = false
//            
//            
//            progress = 0
//            choicesProgress = 0.0
//            factorsProgress = 0.0
//            //self.determineProgress()
//            //self.determineButtonStatesTwo()
//        
//            
//        }))
//
//        
//        self.present(alert, animated: true, completion: nil)
//        
//    }
//    
//
//    @IBAction func newButtonTapped(_ sender: Any) {
//        
//        
//        
//       createAlert(title: "Add decision", message: "Adding a decision will clear your current decision")
//    
//        
//        
//      
//    }
    
    
    

}



extension Notification.Name {
    static let reload = Notification.Name("reload")
    static let pageSwiped = Notification.Name("pageSwiped")
    static let helpPageSwiped =  Notification.Name("helpPageSwiped")
    static let searchOptions = Notification.Name("searchOptions")
   
    
}
