//
//  RankingViewController.swift
//  Choyces
//
//  Created by Amanda Cosel on 5/17/17.
//  Copyright © 2017 DCApps. All rights reserved.
//



import UIKit



class RankingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIPopoverPresentationControllerDelegate  {
    
    
    //OUTLETS
       
    @IBOutlet weak var rankingTable: UITableView!
    
    @IBOutlet weak var evaluatedCriteriaName: UILabel!
    
    //@IBOutlet weak var toolTipButton: UIButton!
    
    //LOCAL VARIABLES
    
    
    var criteriaBeingEvaluated : String = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        rankingTable.delegate = self
        rankingTable.dataSource = self
        
        /*
        
        let origToolTipImage = UIImage(named: "Info-50.png")
        let tintedToolTipImage = origToolTipImage?.withRenderingMode(.alwaysTemplate)
        toolTipButton?.setImage(tintedToolTipImage, for: .normal)
        toolTipButton?.tintColor = UIColor.lightGray
 
 */
        
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(loadView(_:)), name: .pageSwiped , object: nil)
        
    
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        if (currentDecision?.criteria.count)! > currentCriteriaIndex {
         criteriaBeingEvaluated =  (currentDecision?.criteria[currentCriteriaIndex].name)!
        
        evaluatedCriteriaName.text = criteriaBeingEvaluated
            
        } else {}
        
        rankingTable.isEditing = true
        
    }
    

    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        
        
        return (currentDecision!.choices.count)
    }
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "rankingCell") as! RankingTableViewCell
        
        let choice = currentDecision?.choices[indexPath.row]
        
        cell.rankingChoiceLabel.text = choice?.name
        
        
        return cell
    }
    
    
    
    /*
    @IBAction func previousCriteriaButtonTapped(_ sender: Any) {
        
        
       
       
        
        
        let firstChoice = IndexPath(row: 0, section: 0)
        let lastChoice = IndexPath(row:(currentDecision?.choices.count)!-1, section: 0)
        
      
        
        
        
        
        
        if currentCriteriaIndex == 0 {
            
          //  performSegue(withIdentifier: "backToFactors", sender: self)
            
            
            
        } else {
            
           // removeRankChoiceForCriteria(firstChoice: firstChoice, lastChoice: lastChoice)

            
            currentCriteriaIndex = currentCriteriaIndex - 1
            
            self.viewDidLoad()
            self.viewDidAppear(true)
            
            
            
        }

        
        for choice in (currentDecision?.choices)! {
            
            
            
           // choice.determineOverallScore(forChoice: choice)
            
        }
        
        
        
    }
    
    */
    
    func loadView(_ notification: Notification) {
        
        
        self.viewDidAppear(true)
        
        if finishedLoading == 0 {
                
               // let firstChoice = IndexPath(row: 0, section: 0)
               // let lastChoice = IndexPath(row:(currentDecision?.choices.count)!-1, section: 0)
               // rankChoiceForCriteria(firstChoice: firstChoice, lastChoice: lastChoice)
                
 /*
                for choice in (currentDecision?.choices)! {
                    
                    
                    
                    choice.determineOverallScore()
                    
                }
 
 */
            
        }
        
        finishedLoading = finishedLoading + 1
        
       
    
    

    }
    
    /*
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToTip" {
            
            let popoverViewController = segue.destination
            popoverViewController.popoverPresentationController?.delegate = self
            let vc = segue.destination as? ToolTIpViewController
            let pop = vc?.popoverPresentationController
            pop?.delegate = vc as UIPopoverPresentationControllerDelegate?
            
        }
        
    }

    
        
    */
    
    
    /*
    
    @IBAction func nextCriteriaButtonTapped(_ sender: Any) {
        
       
        
        let firstChoice = IndexPath(row: 0, section: 0)
        let lastChoice = IndexPath(row:(currentDecision?.choices.count)!-1, section: 0)
        rankChoiceForCriteria(firstChoice: firstChoice, lastChoice: lastChoice)
        
        
        
        for choice in (currentDecision?.choices)! {
            
            
            
            choice.determineOverallScore()
            
        }
        
        

        
        
        
        if currentCriteriaIndex < ((currentDecision?.criteria.count)! - 1) {
            
            
            
            currentCriteriaIndex = currentCriteriaIndex + 1
            
            self.viewDidLoad()
            self.viewDidAppear(true)
            
            //label = criteriabeingevaluated[+1].name
            
        } else {
            
           
          
           currentCriteriaIndex = currentCriteriaIndex + 1
            
            performSegue(withIdentifier: "goToCalculatedResult", sender: AnyObject.self)
            
            
        
        }
        
    }
 
 */
    
    
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        
        
        return true
    }
    
    
    
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        var itemToMove = currentDecision?.choices[sourceIndexPath.row]
        currentDecision?.choices.remove(at: sourceIndexPath.row)
        currentDecision?.choices.insert(itemToMove!, at: destinationIndexPath.row)
        
        /*
        let firstChoice = IndexPath(row: 0, section: 0)
        let lastChoice = IndexPath(row:(choices.count)-1, section: 0)
        rankChoiceForCriteria(firstChoice: firstChoice, lastChoice: lastChoice)
 
 
 */
 
        rankingTable.reloadData()
        
    }
    
    /*
    func rankChoiceForCriteria ( firstChoice: IndexPath, lastChoice: IndexPath) {
        
        for index in firstChoice.row...lastChoice.row {
//            choiceClass?.choices[index].priority = index + 1
            
            var rankOfChoiceInCriteria = (currentDecision?.choices.count)! - (index)
            
            var newRanking = rankOfChoiceInCriteria * (currentDecision?.criteria[currentCriteriaIndex].priority)!
            
            currentDecision?.choices[index].rankings.append(newRanking)
            
        }

    

   

}
    
 
    
    
    func removeRankChoiceForCriteria(firstChoice: IndexPath, lastChoice: IndexPath) {
        
        for index in firstChoice.row...lastChoice.row {
        currentDecision?.choices[index].rankings.remove(at: currentCriteriaIndex - 1)
            
        }
        
    }
    
    */
    
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        
            return .none
        
    }
    
    /*
   
    func determineOverallScores() {
        
       for choice in (currentDecision?.choices)! {
            
            
           // var choiceBeingSummed = choice
            var choiceIndex: Int = 0
            var overallScore: Int = 0
            for ranking in choice.rankings {
                
            overallScore = overallScore + ranking
                
        }
        
            
            currentDecision?.choices[choiceIndex].overallScore = overallScore
            
            choiceIndex = choiceIndex + 1
        
        print("\(choice.name): \(choice.overallScore)")
            
        }
        
        
        
    }
   
 */
 
    
       
    /*
    
    @IBAction func unwindToRankingController(segue: UIStoryboardSegue) {
        
        let firstChoice = IndexPath(row: 0, section: 0)
        let lastChoice = IndexPath(row:(currentDecision?.choices.count)!-1, section: 0)
        
        currentDecision?.removeRankChoiceForCriteria(firstChoice: firstChoice, lastChoice: lastChoice)
        
        currentCriteriaIndex = currentCriteriaIndex - 1
        
    }

    
    */
    
    
}
