//
//  HelpPageViewController.swift
//  Choyces
//
//  Created by Amanda Cosel on 6/3/17.
//  Copyright © 2017 DCApps. All rights reserved.
//

import UIKit

class HelpPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    
    var pages = [UIViewController]()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        self.dataSource = self
        
        let page1: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "one")
        let page2: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "two")
        let page3: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "three")
        let page4: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "four")
        
        
        pages.append(page1)
        pages.append(page2)
        pages.append(page3)
        pages.append(page4)
        
        setViewControllers([page1], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        
    }
    
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
    guard let viewControllerIndex = pages.index(of: viewController) else {
    return nil
    }
        
    helpPageIndex = viewControllerIndex
    
    NotificationCenter.default.post(name: .helpPageSwiped, object: nil)
    
    let nextIndex = viewControllerIndex + 1
    
    
    guard nextIndex < pages.count else {
    
    
    return nil
    }
    
    guard pages.count > nextIndex else {
    return nil
    }
    
    
    

    

        return pages[nextIndex]
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        
        
        
        guard let viewControllerIndex = pages.index(of: viewController) else {
            return nil
        }
        
        helpPageIndex = viewControllerIndex
        
        NotificationCenter.default.post(name: .helpPageSwiped, object: nil)
        
        let previousIndex = viewControllerIndex - 1
        
        
        
        
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard pages.count > previousIndex else {
            return nil
        }
        
        
        
        return pages[previousIndex]
       
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return pages.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        guard let firstViewController = viewControllers?.first, let firstViewControllerIndex = pages.index(of: firstViewController) else {
            return 0
        }
        
        return firstViewControllerIndex
    }
    
   
}
