//
//  SearchTableViewCell.swift
//  Choyces
//
//  Created by Amanda Cosel on 6/4/17.
//  Copyright © 2017 DCApps. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var choiceCategoryName: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
