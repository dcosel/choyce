//
//  RankingPageViewController.swift
//  Choyces
//
//  Created by Amanda Cosel on 5/27/17.
//  Copyright © 2017 DCApps. All rights reserved.
//

import UIKit


class RankingPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        self.dataSource = self
        
        if let showVC: UIViewController? = controllers[currentCriteriaIndex] {
        
        setViewControllers([showVC!], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
            
           
        
        }
        
       
        
       
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.delegate = self
        self.dataSource = self
        
        //UIPageControl.isSubclass(of: CustomDotsPageControl.self)
        
        
        
    
        //self.CustomDotsPageControl.updateDots(self)
        //determineCurrentIndex()
        
        
        
      // setViewControllers([firstPage], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        
        
         //let currentIndex = controllers.index(of: viewController as! RankingViewController)!
         //currentCriteriaIndex = controllers[currentIndex]
        
       //NotificationCenter.default.post(name: .pageSwiped, object: nil)
        
       
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }

    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for view in self.view.subviews {
            if view is UIScrollView {
                view.frame = UIScreen.main.bounds
            } else if view is UIPageControl {
                
                
                view.backgroundColor = UIColor(colorLiteralRed: 227/255, green: 227/255, blue:
                    227/255, alpha: 1)
            }
            
        }
    }
    
    /*
    func determineCurrentIndex() {
        
        
       var index = 0
        
        for controller in controllers {
            
            if ((controller.isViewLoaded) && (controller.view.window != nil)) == true {
                actualCriteriaIndex = index
            } else {
            index = index + 1
            }
            
        }
 
 

         //futureIndex = currentIndex + 1
       // pastIndex = currentIndex - 1
        
        
        
        print("\(actualCriteriaIndex), \(currentCriteriaIndex)")
       
        
    }
    
   */
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        
        print("get current index")
        
        guard let viewControllerIndex = controllers.index(of: (viewController as! UIViewController)) else {
            return nil
        }
        
        
        current = viewControllerIndex
    
        print("current: \(current)")
        figureOutScores()
        
        //determineCurrentIndex(ofViewController: viewController)
    
        
        let nextIndex = viewControllerIndex + 1
        
   
        
       NotificationCenter.default.post(name: .pageSwiped, object: nil)
        
        
        
        guard nextIndex < controllers.count else {
            

            return nil
        }
        
        guard controllers.count > nextIndex else {
            return nil
        }
        
      
        
        return controllers[nextIndex]
        
        
        
    
    }
    
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        print("get current index")
        
        guard let viewControllerIndex = controllers.index(of: (viewController as! RankingViewController)) else {
            return nil
        }
        
        current = viewControllerIndex
      
        print("current: \(current)")
        figureOutScores()
        
       
        

        let previousIndex = viewControllerIndex - 1
        
       NotificationCenter.default.post(name: .pageSwiped, object: nil)
        
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard controllers.count > previousIndex else {
            return nil
        }
        
       
        
        return controllers[previousIndex]
        



}
    
    
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return controllers.count
    }
    
     

    
    
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        guard let firstViewController = viewControllers?.first, let firstViewControllerIndex = controllers.index(of: firstViewController as! UIViewController) else {
            return 0
        }
        
        return firstViewControllerIndex
    }
 

    
    
    
    func figureOutScores() {
        
        let firstChoice = IndexPath(row: 0, section: 0)
        let lastChoice = IndexPath(row:(currentDecision?.choices.count)!-1, section: 0)

        
        if currentDecision?.choices.first?.overallScore == 0 && current == 0 {
            
            print("do nothing")
            
        } else {
        
        
        if current > previous! {
            
            print("addRankings")
            
            currentDecision?.rankChoiceForCriteria(firstChoice: firstChoice, lastChoice: lastChoice)
            currentCriteriaIndex = currentCriteriaIndex + 1
            rankingsProgress = rankingsProgress + (Float(1)/Float((currentDecision?.criteria.count)!))
           
            /*
            if rankingsProgress == 1.0 {
                
                progress = 5
                
            } else {
                progress = 4
            }
            */
            
            
            
            print(rankingsProgress)
            
            
        } else {
            
            print("deleteRankings")
            currentDecision?.removeRankChoiceForCriteria(firstChoice: firstChoice, lastChoice: lastChoice)
            currentCriteriaIndex = currentCriteriaIndex - 1
            rankingsProgress = rankingsProgress - (Float(1)/Float((currentDecision?.criteria.count)!))
            print(rankingsProgress)
            
            if rankingsProgress == 1.0 {
                
                progress = 5
                
            } else {
                progress = 4
            }
           
        }
        
        
        
        
        
        for choice in (currentDecision?.choices)! {
            
            
            
            choice.determineOverallScore()
            
            
            
            
        }

        }
        
        
    }
    
    
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        finishedLoading = 0
        
        
        
        
        
        let firstChoice = IndexPath(row: 0, section: 0)
        let lastChoice = IndexPath(row:(currentDecision?.choices.count)!-1, section: 0)
        
        print("now I'll ccompare current vs. previous")
        
    
        
        
        
       
        if finished {
            
        previous = controllers.index(of: previousViewControllers.first! as! UIViewController)!
        
           print("previous: \(previous)")
            
            
            
            
            
            
            
        
            
            
            
            

            
            
            
            
            
            

            //determineCurrentIndex()
            
            
            print("now lets do the rankings")
            
            
            
           
            
            
        }
            
            
        
            
            
            
        
        
    }
    
    
    
    
}
