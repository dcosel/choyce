//
//  SideMenuViewController.swift
//  Choyces
//
//  Created by Amanda Cosel on 6/16/17.
//  Copyright © 2017 DCApps. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {

    @IBOutlet weak var userPicImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var signOutButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = activeUser?.name

       
    }
    
    
    @IBAction func signOutButtonTapped(_ sender: Any) {
        
        
        
        performSegue(withIdentifier: "unwindToSignIn", sender: AnyObject.self)
        
        
    }
    
    
    func createAlert(title: String, message: String) {
        
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler: { (action) in
            
            alert.dismiss(animated: true, completion: nil)
            
            
        }))
        
        alert.addAction(UIAlertAction(title: "Proceed", style: .default, handler: { (action) in
            
            
            
            alert.dismiss(animated: true, completion: nil)
            
            
            
            
            var newDecision = Decision(name: "")
            
            currentDecision = newDecision
            decisions.append(newDecision)
            
            self.performSegue(withIdentifier: "unwindToDecision", sender: AnyObject.self)
            usePreLoadedFactors = nil
            //self.yesNo = false
            
            
            progress = 0
            choicesProgress = 0.0
            factorsProgress = 0.0
            //self.determineProgress()
            //self.determineButtonStatesTwo()
            
            
        }))
        
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    @IBAction func newButtonTapped(_ sender: Any) {
        
        
        
        createAlert(title: "Add decision", message: "Adding a decision will clear your current decision")
        
        
        
        
    }

    
    

 
}
