//
//  ToolTIpViewController.swift
//  Choyces
//
//  Created by Amanda Cosel on 6/7/17.
//  Copyright © 2017 DCApps. All rights reserved.
//

import UIKit

class ToolTIpViewController: UIViewController, UIPopoverPresentationControllerDelegate {
    
    var yesNo: Bool = false

    @IBOutlet weak var dontShowCheckBox: UIImageView!
    
    @IBOutlet weak var addOwnButton: UIButton!
    
    @IBOutlet weak var viewPopularButton: UIButton!
    
    
    @IBOutlet weak var toolTipLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        checkboxChecked()
    
        
        addOwnButton.layer.borderColor = UIColor.lightGray.cgColor
        addOwnButton.layer.borderWidth = 1
        addOwnButton.layer.cornerRadius = 5
        
        
        viewPopularButton.layer.borderColor = UIColor.lightGray.cgColor
        viewPopularButton.layer.borderWidth = 1
        viewPopularButton.layer.cornerRadius = 5
        
        
        if progress == 1 {
            
            if ((currentDecision?.name.hasPrefix("should"))! || (currentDecision?.name.hasPrefix("Should"))! || (currentDecision?.name.hasPrefix("do"))! || (currentDecision?.name.hasPrefix("Do"))!) && (currentDecision?.name.range(of:" or ") == nil) {
                
                yesNo = true
                
                toolTipLabel.text = "This looks like a YES/NO decision"
                viewPopularButton.setTitle("That's true, those are my options!", for: .normal)
                
                addOwnButton.setTitle("Not quite, I'll add my own options", for: .normal)
            }
            
            /*
            if (currentDecision?.name.range(of:"should") != nil) || (currentDecision?.name.range(of:"do") != nil) {
                toolTipLabel.text = "YES!"
            
            }
 
 */     else {
                
                yesNo = false
            
            toolTipLabel.text = "You have a decision to make. Would you like to choose from popular options?"
                viewPopularButton.setTitle("Select from popular options", for: .normal)
                addOwnButton.setTitle("No thanks, I'll add my own", for: .normal)
            }
        } else if progress == 2 {
            
            
            if usePreLoadedFactors == true {
                
                toolTipLabel.text = "We've added some popular factors based on what we know about your decision"
                viewPopularButton.setTitle("Perfect, I'll use those", for: .normal)
                addOwnButton.setTitle("No thanks, I'll add my own", for: .normal)
                
            } else {
                
                toolTipLabel.text = "Would you like to choose from popular factors?"
                viewPopularButton.setTitle("Yes, let's browse popular factors", for: .normal)
                addOwnButton.setTitle("No thanks, I'll add my own", for: .normal)
                
            }
          
        }else if progress == 3 {
            
            toolTipLabel.text = "prioritize factors tip"
        }else if progress == 4 {
            
            toolTipLabel.text = "rankings tip"
        } else {}
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    func checkboxChecked() {
        
        if showToolTip == true {
            dontShowCheckBox?.image = dontShowCheckBox?.image!.withRenderingMode(.alwaysTemplate)
            
            dontShowCheckBox?.tintColor = UIColor.darkGray
        } else {
            
            dontShowCheckBox?.image = dontShowCheckBox?.highlightedImage!.withRenderingMode(.alwaysTemplate)
            
            dontShowCheckBox?.tintColor = UIColor.darkGray
        }
    }
    
    
    @IBAction func dontShowButtonTapped(_ sender: Any) {
    
        if showToolTip == true {
        showToolTip = false
            
            checkboxChecked()
            
            self.dismiss(animated: true, completion: nil)
            
        } else {
            
            showToolTip = true
            dontShowCheckBox?.image = dontShowCheckBox?.image!.withRenderingMode(.alwaysTemplate)
            
            dontShowCheckBox?.tintColor = UIColor.darkGray
            
        }
    }
    
    @IBAction func showPopularButtonTapped(_ sender: Any) {
        
        
        if usePreLoadedFactors == true {
            
            self.dismiss(animated: true, completion: nil)
            usePreLoadedFactors = nil
            
        } else {
        
        
        if yesNo == false {
        
        performSegue(withIdentifier: "unwindToViewPopular", sender: yesNo)
            
        } else {
            
            performSegue(withIdentifier: "unwindToViewPopular", sender: yesNo)
            
        }
        
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "unwindToViewPopular"{
            
            
            let nextVC = segue.destination as! MainViewController
            nextVC.yesNo = (sender as? Bool)!
            
        }
    }
    
    
    @IBAction func addOwnButtonTapped(_ sender: Any) {
        
        if usePreLoadedFactors == true {
         usePreLoadedFactors = false
            currentDecision?.criteria = []
            factorsProgress = 0.0
            performSegue(withIdentifier: "unwindToViewPopular", sender: yesNo)
        } else {
        
        
        self.dismiss(animated: true, completion: nil)
        }
    }
}
