//
//  ContainerViewController.swift
//  Choyces
//
//  Created by Amanda Cosel on 5/26/17.
//  Copyright © 2017 DCApps. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {
    
    
    //LOCAL VARIABLES
    
    var vc : UIViewController!
    var segueIdentifier : String!
    var lastViewController: UIViewController!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        segueIdentifierReceivedFromParent(button: "choices")

        
    }
    
    
    
    
    func segueIdentifierReceivedFromParent(button: String){
        if button == "choices"
        {
            
            self.segueIdentifier = "goToChoices"
            self.performSegue(withIdentifier: self.segueIdentifier, sender: nil)
            
        } else if button == "ranking" {
            
            self.segueIdentifier = "goToRankings"
            self.performSegue(withIdentifier: self.segueIdentifier, sender: nil)
            
        } else if button == "factors"
        {
            
            self.segueIdentifier = "goToFactors"
            self.performSegue(withIdentifier: self.segueIdentifier, sender: nil)
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
    

        if segue.identifier == segueIdentifier{
            //Avoids creation of a stack of view controllers
            if lastViewController != nil{
                lastViewController.view.removeFromSuperview()
            }
            //Adds View Controller to Container "first" or "second"
            vc = segue.destination as! UIViewController
            self.addChildViewController(vc)
            vc.view.frame = CGRect(x: 0,y: 0, width: self.view.frame.width,height: self.view.frame.height)
            self.view.addSubview(vc.view)
            vc.didMove(toParentViewController: self)
            lastViewController = vc
            
        }
    
    }


}
