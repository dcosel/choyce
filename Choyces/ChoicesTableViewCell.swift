//
//  ChoicesTableViewCell.swift
//  Choyces
//
//  Created by Amanda Cosel on 5/16/17.
//  Copyright © 2017 DCApps. All rights reserved.
//

import UIKit

class ChoicesTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var grayLine: UIView!
    
    @IBOutlet weak var choiceNameLabel: UILabel!
   
    
    @IBOutlet weak var deleteChoiceButton: UIButton!
  
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
