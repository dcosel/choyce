//
//  ResultsTableViewCell.swift
//  Choyces
//
//  Created by Amanda Cosel on 6/6/17.
//  Copyright © 2017 DCApps. All rights reserved.
//

import UIKit

class ResultsTableViewCell: UITableViewCell {
    @IBOutlet weak var choiceNameLabel: UILabel!
    @IBOutlet weak var resultPercentage: UILabel!
    @IBOutlet weak var resultsImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    

}
