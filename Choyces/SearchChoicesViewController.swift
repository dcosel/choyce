//
//  SearchChoicesViewController.swift
//  Choyces
//
//  Created by Amanda Cosel on 6/4/17.
//  Copyright © 2017 DCApps. All rights reserved.
//

import UIKit
import Firebase

var choicesToAdd: [Choice] = []

class SearchChoicesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var choiceCategory: ChoiceCategory? = nil
    
    let choiceRef = FIRDatabase.database().reference().child("Choices").child("ChoiceCategories")
    
    
    //var choicesToAdd: [Choice] = []
    
    @IBOutlet weak var searchChoicesTable: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchChoicesTable.delegate = self
        searchChoicesTable.dataSource = self
        
        
       
        
        
        self.choiceCategory?.choices = []
        
        
       // self.navigationItem.title = "Search Choice"
        
        choiceRef.child((choiceCategory?.name)!).child("choices").observe(FIRDataEventType.childAdded, with: {(snapshot) in
            
            
            let choice = Choice(name: "")
            
            choice.name = snapshot.key
            
            
            
            self.choiceCategory?.choices.append(choice)
            
            
            
            for currentChoice in (currentDecision?.choices)! {
                
                if currentChoice.isSelected == true {
                    
                    for choice in (self.choiceCategory?.choices)! {
                        
                        if choice.name == currentChoice.name {
                            choice.isSelected = true
                        }
                    }
                    
                }
            }

            
            
            self.searchChoicesTable.reloadData()
            
           // print(snapshot)
            
            
        })

        

    }
    
    override func viewDidDisappear(_ animated: Bool) {
       
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
  

    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return "\((choiceCategory?.name)!)"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (choiceCategory?.choices.count)!
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchChoiceCell") as! SearchChoicesTableViewCell
        
        
        var searchChoice = choiceCategory?.choices[indexPath.row]
        
        var searchChoiceName = (searchChoice?.name)!
        
        cell.searchChoicesNameLabel.text! =  "\(searchChoiceName)"
        
        
        if searchChoice?.isSelected == true {
            cell.accessoryType = .checkmark
        } else {
            
            cell.accessoryType = .none
        }

        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchChoiceCell") as! SearchChoicesTableViewCell
        let choiceToAdd = choiceCategory?.choices[indexPath.row]
        
        
        
        if choiceToAdd?.isSelected
            == true {
            
            var choiceIndex = 0
            var choiceIndexTwo = 0
            
            choiceToAdd?.isSelected = false
            
            
            for choice in (currentDecision?.choices)! {
                
                if choice.name == choiceToAdd?.name {
                    currentDecision?.choices.remove(at: choiceIndexTwo)
                } else {
                    choiceIndexTwo = choiceIndexTwo + 1
                }
                
                
            }
            
            
            for choice in choicesToAdd {
                
                if choice.name == choiceToAdd?.name {
                    
                    choicesToAdd.remove(at: choiceIndex)
                    
                } else {
                    choiceIndex = choiceIndex + 1
                }
                
            }
            
            
            
        } else {
            choiceToAdd?.isSelected = true
            choicesToAdd.append(choiceToAdd!)
            
        }

        
        
      
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .checkmark{
                cell.accessoryType = .none
            }
            else{
                cell.accessoryType = .checkmark
            }
        }
        
        print(choicesToAdd)
        
        NotificationCenter.default.post(name: .searchOptions, object: nil)
        
    }

    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let choiceToRemove = choiceCategory?.choices[indexPath.row]
        
        
        
       // choicesToAdd.remove(at: indexPath.row)
        
        print(choicesToAdd)
    }
    
    


}
