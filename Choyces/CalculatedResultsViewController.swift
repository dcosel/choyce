//
//  CalculatedResultsViewController.swift
//  Choyces
//
//  Created by Amanda Cosel on 6/6/17.
//  Copyright © 2017 DCApps. All rights reserved.
//

import UIKit

class CalculatedResultsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var makeAnotherDecisionButton: UIButton!
    @IBOutlet weak var resultsTable: UITableView!

    @IBOutlet weak var winnerDescriptionLabel: UILabel!
    
    @IBOutlet weak var winnerNameLabel: UILabel!
    //@IBOutlet weak var toolTipButton: UIButton!
    
    var bestScore: Int = 18
     var choices: [Choice] = (currentDecision?.choices)!
    var winner: String = ""
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(showCalculatedWinner(_:)), name: .pageSwiped , object: nil)
        
        resultsTable.delegate = self
        resultsTable.dataSource = self
        
        /*
        let origToolTipImage = UIImage(named: "Info-50.png")
        let tintedToolTipImage = origToolTipImage?.withRenderingMode(.alwaysTemplate)
        toolTipButton?.setImage(tintedToolTipImage, for: .normal)
        toolTipButton?.tintColor = UIColor.lightGray
 
 */
        
        choices = (currentDecision?.choices)!
        
       // determineBestPossibleScore()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (choices.count)
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "resultsCell") as! ResultsTableViewCell
        var choice = choices[indexPath.row]
        
        var choiceName = (choice.name)
        
        cell.choiceNameLabel.text! =  "\(choiceName)"
        
        print(bestScore)
        
        var percentage: Int = 0
        
        
        
       //percentage = Int((Double((choice.overallScore)) / Double(bestScore)) * 100)
        if (currentDecision?.choices.count)! > 3 {
        
        for index in controllers {
            
            if indexPath.row == 0 {
                cell.resultPercentage.text = "Best"
                cell.resultPercentage.textColor = UIColor.green
            } else if indexPath.row == 1 {
                    cell.resultPercentage.text = "Better"
                cell.resultPercentage.textColor = UIColor.orange
            } else if indexPath.row == 2 {
                cell.resultPercentage.text = "Good"
                cell.resultPercentage.textColor = UIColor.yellow
            } else {
                 cell.resultPercentage.text = ""
            }
            }
        } else {
            for index in controllers {
                
                if indexPath.row == 0 {
                    cell.resultPercentage.text = "Best"
                    cell.resultPercentage.textColor = UIColor.green
                } else if indexPath.row == 1 {
                    cell.resultPercentage.text = "Good"
                    cell.resultPercentage.textColor = UIColor.orange
                } else {
                    cell.resultPercentage.text = ""
                }
            }
            
            
            
        }
        
        
        //cell.resultPercentage.text = "\(percentage)%"
        
        if indexPath.row == 0 {
            cell.resultsImage.isHidden = false
            
        } else {
            
            cell.resultsImage.isHidden = true
        }
        
        
        return cell
    }
    
    
    func showCalculatedWinner(_ notification: Notification) {
        
        
        choices = (currentDecision?.choices)!
        
       // determineBestPossibleScore()
        
        
        
        
        resultsTable.reloadData()
        
        choices.sort(by: {
            $0.overallScore > $1.overallScore
        })
        
        
        determineWinner()
        if choices.count > 1{
               
        if ((Double(choices[choices.count - 1].overallScore) / Double(bestScore)) - (Double(choices[choices.count - 2].overallScore) / Double(bestScore))) < 0.1 {
            winnerDescriptionLabel.text = "That was a close one..."
            winnerNameLabel.text = "But you should probably choose \(winner)"
            
            
        } else {
        
        winnerDescriptionLabel.text = "This is a no-brainer!"
        winnerNameLabel.text = "\(winner) is the clear choice"
      
        }
            
        } else {
            
            winnerDescriptionLabel.text = "That wasn't much of a decision"
            winnerNameLabel.text = "\(winner) didn't have any competition"
        }
    }
    
    
    @IBAction func makeAnotherDecisionButtonTapped(_ sender: Any) {
        
         createAlert(title: "Add decision", message: "Adding a decision will clear your current decision")
        
        
    }
    
    
    
    func createAlert(title: String, message: String) {
        
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler: { (action) in
            
            alert.dismiss(animated: true, completion: nil)
            
            
        }))
        
        alert.addAction(UIAlertAction(title: "Proceed", style: .default, handler: { (action) in
            
            
            
            alert.dismiss(animated: true, completion: nil)
            
            
            
            
            var newDecision = Decision(name: "")
            
            currentDecision = newDecision
            decisions.append(newDecision)
            
            self.performSegue(withIdentifier: "unwindToDecision", sender: AnyObject.self)
            usePreLoadedFactors = nil
           
            
            
            progress = 0
            choicesProgress = 0.0
            factorsProgress = 0.0
            //self.determineProgress()
            //self.determineButtonStatesTwo()
            
            
        }))
        
        
        self.present(alert, animated: true, completion: nil)
        
    }

    
    
    func determineWinner() {
        
        var winningScore: Int = 0
        
        
        for choice in (currentDecision?.choices)! {
            
            var isWinningChoice = choice
            
            if isWinningChoice.overallScore > winningScore {
                winningScore = isWinningChoice.overallScore
                winner = isWinningChoice.name
            } else {
                print("nope")
            }
            
        }
        //print(winner)
       // print(winningScore)
       // winnerNameLabel.text = "The winner is: \(winner)!"
        
    }
    
    /*
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToTip" {
            
            let popoverViewController = segue.destination
            popoverViewController.popoverPresentationController?.delegate = self
            let vc = segue.destination as? ToolTIpViewController
            let pop = vc?.popoverPresentationController
            pop?.delegate = vc as UIPopoverPresentationControllerDelegate?
            
        }
        
    }
    
*/

    /*
    
    func determineBestPossibleScore() {
        
        bestScore = 0
        var currentCriteriaIndex = (currentDecision?.criteria.count)!
        
        for criteria in (currentDecision?.criteria)! {
            
            bestScore = ((currentDecision?.choices.count)! * currentCriteriaIndex) + bestScore
            
            currentCriteriaIndex = currentCriteriaIndex - 1
            
        }
        
    }
    
    */

}
