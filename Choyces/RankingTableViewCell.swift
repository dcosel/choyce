//
//  RankingTableViewCell.swift
//  Choyces
//
//  Created by Amanda Cosel on 5/17/17.
//  Copyright © 2017 DCApps. All rights reserved.
//

import UIKit

class RankingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var rankingChoiceLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
