//
//  SignInViewController.swift
//  Choyces
//
//  Created by Amanda Cosel on 6/13/17.
//  Copyright © 2017 DCApps. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import Cheetah
import FacebookLogin
import FacebookCore


var activeUser: User? = nil
    

var fbLoginSuccess = false

class SignInViewController: UIViewController {
    
    
    var ref : FIRDatabaseReference!
    
    

    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var signInButton: UIButton!
    
    @IBOutlet weak var errorMessageLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
        // Add a custom login button to your app
        let myLoginButton = UIButton(type: .custom)
        myLoginButton.backgroundColor = UIColor.darkGray
       
        myLoginButton.frame = CGRect(x: 0, y: 0, width: 180, height: 40)
        myLoginButton.center = view.center
        
        myLoginButton.setTitle("Login", for: .normal)
        
        // Handle clicks on the button
        
        
        myLoginButton.addTarget(self,action:#selector(self.loginButtonTapped), for: .touchUpInside)
        
        // Add the button to the view
        view.addSubview(myLoginButton)
        
        
      //  SDKSettings.appId = "com.dcapps.Choyces"
        
//        let loginButton = LoginButton(readPermissions: [ .publicProfile, .email])
//        loginButton.center = view.center
//        
//        view.addSubview(loginButton)
        
//
      //  taskCheckBox.addTarget(self,action:#selector(taskCompleted(sender:)), for: .touchUpInside)
        
        
      //LoginButton.addTarget(self,action:#selector(self.loginButtonTapped), for: .touchUpInside)
        
        
        
              self.errorMessageLabel.text = ""
        
        signInButton.layer.borderColor = UIColor.lightGray.cgColor
        signInButton.layer.borderWidth = 1
        signInButton.layer.cornerRadius = 5

        
        ref = (FIRDatabase.database().reference().child("Users"))
    

       
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        
       
            if AccessToken.current != nil {
                
                
                
            // User is logged in, use 'accessToken' here.
//            self.performSegue(withIdentifier: "signInSegue", sender: nil)
            activeUser = User(uid: (AccessToken.current?.userId)!)
                
                print(activeUser)
                let credential = FIRFacebookAuthProvider.credential(withAccessToken: (AccessToken.current?.authenticationToken)!)
                
                FIRAuth.auth()?.signIn(with: credential, completion: { (user, error) in
                        
                        if error != nil {
                            
                            print("we have an errror: \(error)")
                            
                            
                            FIRAuth.auth()?.createUser(withEmail: self.emailTextField.text!, password: self.passwordTextField.text!, completion: { (user, error) in

                                if error != nil {
                                    
                                    print("I can't do anything!")
                                     print("we have an errror: \(error)")
                                }else {
                                    
                                   
                                    
                                    activeUser?.email = (user?.email)!
                                    activeUser?.name = (user?.displayName)!
                                    activeUser?.uid = (user?.uid)!
                                    activeUser
                                    
                                    self.ref.setValue(activeUser?.uid)
                                    self.ref.child((activeUser?.uid)!).child("email").setValue(activeUser?.email)
                                    
                                    self.ref.child((activeUser?.uid)!).child("name").setValue(activeUser?.name)
                                    
                                    
                                    self.performSegue(withIdentifier: "signInSegue", sender: nil)
                                    
                                }})} else {
                            
                            activeUser?.email = (user?.email)!
                            activeUser?.name = (user?.displayName)!
                            activeUser?.uid = (user?.uid)!
                            
                            
                            
                            self.ref.child((activeUser?.uid)!).child("email").setValue(activeUser?.email)
                            
                            self.ref.child((activeUser?.uid)!).child("name").setValue(activeUser?.name)
                            
                            self.performSegue(withIdentifier: "signInSegue", sender: nil)
                            print("Welcome: \(user?.displayName)")
                            
                        }
            })
                
            }else {
                print ("howdy ho")
        }
        
        
        
        }
    
    
    
                        

    
    
    
    
//    func loginButton(loginButton: LoginButton!, didCompleteWithResult result: LoginResult!, error: NSError!) {
//        print("User Logged In")
//        
//        if ((error) != nil) {
//            // Process error
//            print(error)
//        }
////        else if result. {
////            // Handle cancellations
////        }
//        else {
//            fbLoginSuccess = true
//            self.viewDidAppear(true)
//            // If you ask for multiple permissions at once, you
//            // should check if specific permissions missing
//            
//        }
//    }
    
    
    func loginButtonTapped() {
        
        
        let loginManager = LoginManager()
        loginManager.logIn([ .publicProfile, .email, .publicProfile], viewController: self) { (LoginResult) in
        
            switch LoginResult {
        
            case .failed(let error):
                print("the error is: \(error)")
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                
                activeUser = User(uid: (AccessToken.current?.userId)!)
                
                
                print("Logged in!")
                
                
                
            }
           self.viewDidAppear(true)
        
    }
    }
    
    
     @IBAction func unwindToSignIn(segue: UIStoryboardSegue) {
        
        
        activeUser = nil
        currentDecision = nil
        
    }
    
    
    
    @IBAction func useWithoutAccountTapped(_ sender: Any) {
        
        self.performSegue(withIdentifier: "signInSegue", sender: nil)
        
    }
    
 
    
    


    @IBAction func signInTapped(_ sender: Any) {
        
      
    FIRAuth.auth()?.signIn(withEmail: emailTextField.text!, password: passwordTextField.text!, completion: { (user, error) in
        
        print("we tried to sign in")
        if error != nil {
            
            print("we have an errror: \(error)")
            
            
            self.errorMessageLabel.text = (error?.localizedDescription)
            
            FIRAuth.auth()?.createUser(withEmail: self.emailTextField.text!, password: self.passwordTextField.text!, completion: { (user, error) in
                
                print("we tried to create a user")
                
                if error != nil {
                    
                    self.signInButton.cheetah
                        .move(10, 0).duration(0.1)
                        
                        .wait()
                        .move(-10, 0).duration(0.1)
                        .wait()
                        .move(10, 0).duration(0.1)
                        .wait()
                        .move(-10, 0).duration(0.1)
                        
                        .run()

                    self.errorMessageLabel.text = (error?.localizedDescription)
                    print("we have an errror: \(error)")
                }else {
                print("created a user")
                    
                    self.ref.child(user!.uid).child("email").setValue(user?.email!)
                    
                    activeUser = User(uid: user!.uid)
                    
                    
                    self.performSegue(withIdentifier: "signInSegue", sender: nil)
                }
                
            })
            
        } else {
            print("sign in successful")
            
            
            activeUser = User(uid: user!.uid)
            self.performSegue(withIdentifier: "signInSegue", sender: nil)
        }
        
    })
        
        
    }
    
    
    

}
