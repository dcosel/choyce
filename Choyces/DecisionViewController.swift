//
//  DecisionViewController.swift
//  Choyces
//
//  Created by Amanda Cosel on 5/21/17.
//  Copyright © 2017 DCApps. All rights reserved.
//

import UIKit
import Cheetah


class DecisionViewController: UIViewController {
    @IBOutlet weak var folderImage: UIImageView!
    
    @IBOutlet weak var decisionTextFieldTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var infoButton: UIButton!
    
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var decisionTextField: UITextField!
    
    @IBOutlet weak var maskView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        maskView.isHidden = true
        
        
       
        
        progress = 0
        
        decisionTextFieldTrailing.constant = 20
        
        decisionTextField.text = currentDecision?.name
        
        if decisionTextField.text == "" {
            
            nextButton.isHidden = true
           // nextLabel.isHidden = true
            
        } else {
            nextButton.isHidden = false
            
            nextButton?.cheetah
                .move(10, 0).duration(0.5)
                .wait()
                .move(-10, 0).duration(0.5)
                .wait(0.5)
               .run().forever
           // nextLabel.isHidden = false
            
        }


        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
         folderImage?.image = folderImage?.image!.withRenderingMode(.alwaysTemplate)
        
        folderImage.tintColor = UIColor.lightGray
        
        
        
        
        if hasLaunched == false {
            maskView.isHidden = false
            let origInfoImage = UIImage(named: "Help Filled-50.png")
            let tintedInfoImage = origInfoImage?.withRenderingMode(.alwaysTemplate)
            infoButton?.setImage(tintedInfoImage, for: .normal)
            infoButton?.tintColor = UIColor.lightGray
            performSegue(withIdentifier: "goToHelp", sender: AnyObject.self)
        }else {
            maskView.isHidden = true
            let origInfoImage2 = UIImage(named: "Help-50.png")
            let tintedInfoImage2 = origInfoImage2?.withRenderingMode(.alwaysTemplate)
            infoButton?.setImage(tintedInfoImage2, for: .normal)
            infoButton?.tintColor = UIColor.lightGray
        }

    }
    
    
    @IBAction func goButtontapped(_ sender: Any) {
        
        
        if decisionTextField.text == currentDecision?.name {
            
            print("Its the same decision")
            
        } else {
        
        var newDecision = Decision(name: "\(decisionTextField.text!)")
        
        currentDecision = newDecision
        decisions.append(newDecision)
            decisionTextField.text = ""
    
        }
        
        progress = 1
        performSegue(withIdentifier: "goToChoices", sender: self)
        
    }
    
    @IBAction func decisionNameChanged(_ sender: Any) {
        
        if decisionTextField.text == "" {
            decisionTextFieldTrailing.constant = 20
           // nextLabel.isHidden = true
            nextButton.isHidden = true
            
        } else {
            //nextLabel.isHidden = false
            
            decisionTextFieldTrailing.constant = 40
            nextButton.isHidden = false
            nextButton?.cheetah
                .move(10, 0).duration(0.5)
                .wait()
                .move(-10, 0).duration(0.5)
                .wait(0.5)
                .run().forever
            
        }
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        decisionTextField.resignFirstResponder()
        return (true)
        
    }
    
    
    @IBAction func unwindToDecisionView(segue: UIStoryboardSegue) {
        
        maskView.isHidden = true
        
        let origInfoImage2 = UIImage(named: "Help-50.png")
        let tintedInfoImage2 = origInfoImage2?.withRenderingMode(.alwaysTemplate)
        infoButton?.setImage(tintedInfoImage2, for: .normal)
        infoButton?.tintColor = UIColor.lightGray
    
        
    }
    
    
    @IBAction func infoButtonTapped(_ sender: Any) {
        
        performSegue(withIdentifier: "goToHelp", sender: AnyObject.self)
        
        maskView.isHidden = false
        let origInfoImage2 = UIImage(named: "Help Filled-50.png")
        let tintedInfoImage2 = origInfoImage2?.withRenderingMode(.alwaysTemplate)
        infoButton?.setImage(tintedInfoImage2, for: .normal)
        infoButton?.tintColor = UIColor.lightGray
        
    }
    
    
   
    

   
}
