//
//  SearchContainerViewController.swift
//  Choyces
//
//  Created by Amanda Cosel on 6/4/17.
//  Copyright © 2017 DCApps. All rights reserved.
//

import UIKit

class SearchContainerViewController: UIViewController {
    @IBOutlet weak var searchView: UIView!

    @IBOutlet weak var closeButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        closeButton.setTitle("Cancel", for: .normal)

        
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(changeButtonStates(_:)), name: .searchOptions , object: nil)
        
        searchView.layer.cornerRadius = 10

        // Do any additional setup after loading the view.
    }
    
    func changeButtonStates(_ notification: Notification) {
        
        if factorsToAdd.count > 0 || choicesToAdd.count > 0 {
            
            
            closeButton.setTitle("Done", for: .normal)
        } else {
            
            closeButton.setTitle("Cancel", for: .normal)
        }
        
        
    }
    
    
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        
        performSegue(withIdentifier: "unwindToMainController", sender: AnyObject.self)
        
        
        
        
    }
    
   

  

}
