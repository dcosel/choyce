//
//  HelpViewController.swift
//  Choyces
//
//  Created by Amanda Cosel on 6/3/17.
//  Copyright © 2017 DCApps. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {
    
    //OUTLETS
    @IBOutlet weak var helpCheckMark: UIImageView!

    @IBOutlet weak var helpCircleView: RandomButtonView!
    @IBOutlet weak var helpView: UIView!
    
    @IBOutlet weak var helpProgressBar: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(incrementHelpProgress(_:)), name: .helpPageSwiped , object: nil)
        
         helpCheckMark?.image = helpCheckMark?.image!.withRenderingMode(.alwaysTemplate)
        
        helpCheckMark.tintColor = UIColor.lightGray
    
        
        
        helpView.layer.cornerRadius = 10

        // Do any additional setup after loading the view.
    }

   
    @IBAction func closeButtonTapped(_ sender: Any) {
        
        performSegue(withIdentifier: "unwindToDecisionView", sender: AnyObject.self)
        
    }
    
    func incrementHelpProgress(_ notification: Notification) {
        
        helpProgressBar.progress = Float(Double(helpPageIndex) * 0.34)
        
        print(helpProgressBar.progress)
        
        if helpProgressBar.progress == 1.0 {
            
            helpCircleView.mainColor = UIColor.green
            
            helpCircleView.ringColor = UIColor.green
            
            helpCheckMark.tintColor = UIColor.white
            
        } else {
            
            helpCircleView.mainColor = UIColor.white
            
            helpCheckMark.tintColor = UIColor.lightGray
            helpCircleView.ringColor = UIColor.lightGray
            
        }
        
    }
    

}
